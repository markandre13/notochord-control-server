# Notochord control server app v0.1.2

## Dev installation:

_Might take some time to complete on a Raspberry pi because pip will probably compile some of the dependencies_ 

- `pipenv install`

## Notochord module compilation

- if you already have a compiled version of the module somewhere just create a symlink to it:
**If you do this don't push those changes to git!!**

```bash
rm -rf notochord-module
ln -s <location of your notochord-module repository> .
```

- otherwise you need to update the submodule and compile (some of the steps might require some time to complete)

```bash
git submodule init
git submodule update --recursive
cd notochord-module
make setup
make
```

## Gunicorn installation

```
sudo apt install python3.7-venv
pipenv lock -r > requirements.txt
python3.7 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
pip install .  # install your application
pip install gunicorn
```

## Gunicorn run on startup

add these lines to `/etc/rc.local`

```
cd /home/pi/notochord-control-server/
venv/bin/gunicorn -b '0.0.0.0:8000' -w 1 -D 'wsgi:application' --threads 1 --access-logfile /home/pi/gunicorn_acc.log --error-logfile  /home/pi/gunicorn_err.log > /home/pi/rc.log 2> /home/pi/rc.err
```

## Dev server:

Only local version in port 5000:
- `pipenv run dev_server`

Remote with no reloads in port 5001
- `pipenv run dev_server_remote`