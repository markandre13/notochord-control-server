import logging
from urllib import request
from flask import Flask, request, Response
from lxml import etree
import os
from sys import path as syspath
syspath.append(os.path.realpath(os.path.join(os.path.dirname(__file__), "../notochord-module/dist")))
from . import utils
from . import endpoints
from . import frontend
from . constants import __version__, version

import notochord
from notochord.core import init_websocket



def create_app(test_config=None):
    # initialize the websocket
    init_websocket()
    
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True, static_url_path='/static')
    app.config.from_mapping(
        SECRET_KEY='dev'
    )

    # catch general errors
    @app.errorhandler(Exception)
    def handle_exception(e):
        logging.exception(e)
        res = utils.get_error_xml("Internal server error", str(e), e)
        return Response(res, mimetype='text/xml'),500
    
    # catch not found error
    @app.errorhandler(404)
    def page_not_found(e):
        res = utils.get_response_xml("Not found")
        return Response(res, mimetype='text/xml'),404

    @app.after_request
    def allow_origin(response):
        response.headers["Access-Control-Allow-Origin"] = "*"
        return response

    app.register_blueprint(endpoints.state.bp)
    app.register_blueprint(endpoints.configuration.bp)
    app.register_blueprint(endpoints.run.bp)
    app.register_blueprint(endpoints.maintenance.bp)
    app.register_blueprint(endpoints.pose.bp)
    
    app.register_blueprint(frontend.bp)

    return app

