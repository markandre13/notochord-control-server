import notochord
from os import path
import re as _re

__version__ = '0.1.2'
version=__version__.split(".")

CHORD_APP_DIR = path.realpath(path.join(path.dirname(__file__), "../.."))
CHORD_MAINTENANCE_DIR = path.join(CHORD_APP_DIR, "notochord-os/maintenance/")
FILES_PATH = path.realpath(path.join(path.dirname(__file__), "files/configurations")) # TODO: Files should be files/, make another constant for configurations
RECORDINGS_PATH = path.realpath(path.join(path.dirname(__file__), "files/recordings"))
ANIMATIONS_PATH = path.realpath(path.join(path.dirname(__file__), "files/animations"))
STATE_PATH = path.realpath(path.join(path.dirname(__file__), "state.json"))
SCHEMA_PATH = path.join(notochord.__path__[0], "Chordata.xsd")
DEFAULT_BIPED_PATH = path.realpath(path.join(path.dirname(__file__), "files/default_biped.xml"))
DEFAULT_BIPED_FORBIDDEN_PATH = path.realpath(path.join(path.dirname(__file__), "../test/files/default_biped_forbidden.xml"))
DEFAULT_BIPED_ERROR_PATH = path.realpath(path.join(path.dirname(__file__), "../test/files/default_biped_error.xml"))

CHORD_CONF_INI = path.join(CHORD_APP_DIR, "notochord-os/chordata_conf.ini")

def _extract_conf_dir():
    """This functions obtains the location of the main configuration file from the
    notochord OS, setting the global CHORD_CONF_DIR constant"""
    with open(CHORD_CONF_INI) as conf_ini:
        _compiled = _re.compile(r'CHORD_CONF_DIR=(.*)$')
        lines = conf_ini.readlines()
        for line in lines:
            res = _compiled.match(line)
            if res:
                globals()["CHORD_CONF_DIR"] = res.group(1)

def _extract_state_vars():
    """This functions obtains the Chordata state variables from the
    notochord OS, it sets each variable it founds there as a global constant"""
    with open(path.join(CHORD_CONF_DIR,"noto_os_state.sh")) as conf_state:
        _compiled = _re.compile(r'\W*export\W*([\w_]+)=\'?([\d\.\w\# ]*)\'?')
        lines = conf_state.readlines()
        for line in lines:
            res = _compiled.match(line)
            if res:
                globals()[res.group(1)] = res.group(2)

try:
    if not path.isfile(CHORD_CONF_INI):
        raise
    else:
        _extract_conf_dir()

    if "CHORD_CONF_DIR" in globals().keys():
        _extract_state_vars()
except Exception as e:
    CHORD_CONF_INI = ""

