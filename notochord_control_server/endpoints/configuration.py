import logging
from urllib import request
from flask import Flask, request, Response, Blueprint
from lxml import etree
import os
from os import path
from notochord_control_server import utils, constants
from time import sleep
import threading

from notochord.timer import Node_Scheduler
from notochord.core import setup, get_scheduler, status
from notochord.communicator import Output_Sink, info, error, trace, debug, flush
from notochord.config import Notochord_Config
from notochord.node import Branch
from notochord.types import KC_STATUS
from notochord.parse import validate_xml
from notochord.sensor_calibration import Sensor_Calibrator

from . import run

bp = Blueprint('configuration', __name__, url_prefix='/configuration')

# configuration
@bp.route('<label>.xml', methods=['GET','POST','DELETE'])
def configuration(label):
	if request.method == 'GET':
		config = utils.read_config(label)
		if not config: # not found error handled on utils.read_config
			res = utils.get_response_xml("Not found")
			return Response(res, mimetype='text/xml'),404
		else: # config found, check if valid

			validation = validate_xml(config)
			if validation[0]:
				config_string = etree.tostring(config, pretty_print=True)
				return Response(config_string, mimetype='text/xml'),200
			else:
				log = validation[1]
				error = log.last_error
				res = utils.get_error_xml("Bad request", error.message)
				return Response(res, mimetype='text/xml'),400
		

	if request.method == 'POST':
		content_raw = request.data
		# parse from string to xml
		try:
			content = etree.fromstring(content_raw)
		except etree.XMLSyntaxError as e:
			res = utils.get_error_xml("Bad request", e.msg, e)
			return Response(res, mimetype='text/xml'),400

		et = etree.ElementTree(content)
		validation = validate_xml(et)
		if not validation[0]:
			print(validation)
			res = utils.get_error_xml("Bad request", "Invalid XML", validation[1])
			return Response(res, mimetype='text/xml'),400
			
		# Try to write to file and catch permission errors
		try:
			file_path = path.join(constants.FILES_PATH, "%s.xml" % label)
			with open(file_path, 'w') as file:
				et.write(file_path)
			res = utils.get_response_xml("Configuration saved successfully")
			return Response(res, mimetype='text/xml'),200
		except PermissionError as e:
			res = utils.get_error_xml("Forbidden", e.strerror)
			return Response(res, mimetype='text/xml'),403


	if request.method == 'DELETE':
		try:
			file_path = path.join(constants.FILES_PATH, "%s.xml" % label)
			os.remove(file_path)
			res = utils.get_response_xml("Configuration deleted successfully")
			return Response(res, mimetype='text/xml'),200
		except Exception as e:
			logging.exception(e)
			res = utils.get_response_xml("Forbidden - %s" % e.strerror)
			return Response(res, mimetype='text/xml'),403
		
@bp.route('set-i2c-addr', methods=['GET'])
def i2c():
	addr = request.args.get('addr',"40")

	try:
		addr = int(addr, 16)
		#if addr < 0x40 or addr > 0x5f:
		#    raise ValueError("Invalid i2c address {:#04x}. Must be between 0x40 and 0x5f.", addr)
	except ValueError as e:
		res = utils.get_error_xml("Bad Request", str(e))
		return Response(res, mimetype='text/xml'),400
	
	if status() == "RUNNING":
		res = utils.get_error_xml("Bad Request", "Cannot change i2c address while running")
		return Response(res, mimetype='text/xml'),400
	
	c = Notochord_Config()
	if status() != "CONFIGURED":
		setup(c, False) # Keep communicator as is
	else: 
		setup(c, True)

	s = Node_Scheduler()
	sensors = s.scan()
	
	i = 0
	
	for sensor in reversed(sensors):
		if isinstance(sensor, Branch):
			sensor.bang()
		else:
			# sensor.setup()
			sensor.bang()
			try:
				sensor.change_addr(addr)
				i += 1
			except ValueError as e:
				res = utils.get_error_xml("Bus communicator error while setting i2c address", str(e))
				return Response(res, mimetype='text/xml'),500
	   
	label = "Changed {} sensors with new address: 0x{:0x}".format(i, addr)
	res = utils.get_response_xml(label)
	return Response(res, mimetype='text/xml'),200

s_calib = None

def calibration_step(step):
	try:
		# Check if notochord is CONFIGURED
		global s_calib
		print(status())
		if status() != "CONFIGURED":
			run.setup_notochord()
			s = Node_Scheduler()
			sensors = None
			sensors = s.scan()
			if s_calib is None:
				s_calib = Sensor_Calibrator(sensors)
		
		if step == 0:
			# Gyro calibration
			info("Test")
			s_calib.calibrate_g()			
			info("Gyro calib done, taking first sample for accel calib..")
			step = 1 # We don't stop and prompt here because this orientation is also useful for accel calib

		if step == 1:
			# Accel calibration
			r = s_calib.get_x_orientation()
			buffer = tuple(s_calib.kc.kalman.accel_buf.fGsStored)
			info("Accel orientation {}/{} stored".format(*s_calib.accel_orientations))
			trace("{:06b} | {}".format(s_calib.kc.kalman.accel_buf.iStoreFlags, buffer[:r+1]))
			if s_calib.accel_buf_complete: # No more samples needed
				info("Accel calibration samples ready, running accel calib..")
				if not s_calib.calculate_x_calib():
					error("Not enough samples to calculate accel calibration")
				else:
					info("Accel offset: {}", s_calib.kc.kalman.accel_cal.fV)
					info("Accel matrix: {}", s_calib.kc.kalman.accel_cal.finvW)

				info("Raise the KC from the table and press MAG when you are ready to do the Mag calibration")

		if step == 2:
			# Mag calibration
			s_calib.calibrate_m()
			info("Mag offset: {}", s_calib.kc.kalman.mag_cal.fV)
			info("mag matrix: {}", s_calib.kc.kalman.mag_cal.finvW)
			info("Calibration complete, press SAVE to save it")

		if step == 3:
			# Save calibration
			s_calib.kc.save_data()
			s_calib.save_calib_json(get_scheduler().sensors, s_calib.json_file)
			debug("Write sensor calib at {}", s_calib.json_file)
	
		flush()
	except Exception as e:
		error("Error on calibration step {}", str(e))
		flush()

@bp.route('calibrate', methods=['GET'])
def calibrate():
	try:
		info("HELLO --------------------------------")
		flush()
		step = request.args.get('step', 0)
		# 0: Gyro calibration
		# 1: Accel calibration
		# 2: Mag calibration
		# 3: Save calibration

		try:
			step = int(step)
			if step < 0 or step > 4: 
				raise ValueError("The requested state ({}) is out of range".format(step))
		except ValueError as e:
			res = utils.get_error_xml("Bad Request", str(e))
			return Response(res, mimetype='text/xml'),400
	
		label = "Starting calibration step {}".format(step)

		# Start a new thread for calibration
		th = threading.Thread(target=calibration_step, args=(step,), daemon=True)
		th.start()
		
		res = utils.get_response_xml(label)
		return Response(res, mimetype='text/xml'),202

	except Exception as e:
		res = utils.get_error_xml("Error on calibration", str(e))
		return Response(res, mimetype='text/xml'),500
	
# get a list of all sensor names
@bp.route('sensors', methods=['GET'])
def sensors():
	try:
		sensors = get_scheduler().sensors
		res = utils.get_sensors_xml(sensors)
		return Response(res, mimetype='text/xml'),200
	except Exception as e:
		res = utils.get_error_xml("Error on getting sensors", str(e))
		return Response(res, mimetype='text/xml'),500