#!/bin/bash

set -eu

CODE_FILE=/etc/chordata/countrycode
if [ -f "$CODE_FILE" ]; then
	read -r code < "$CODE_FILE"
	echo "$code"
else
	echo ""
fi