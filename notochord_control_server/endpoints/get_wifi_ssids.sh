#!/bin/bash

set -eu

if wpa_cli -i wlan0 status 2>/dev/null; then 
	wpa_cli list_networks | grep -v "Selected interface" | grep -v "network id";
else 
	ssid_list=$(cat /etc/wpa_supplicant/wpa_supplicant.conf | grep ssid= | sed 's/ssid=\"\(.*\)\"/\1/');
	counter=0
	for s in $ssid_list; do echo -e "$counter\t$s";counter=$((counter+1)); done
fi