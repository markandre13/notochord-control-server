from ctypes import util
import logging
from urllib import request
from flask import Flask, request, Response, Blueprint, jsonify
from lxml import etree
from ptyprocess import PtyProcessUnicode
import subprocess
import re
import threading
import queue

from notochord_control_server import utils, constants
from os import path

bp = Blueprint('maintenance', __name__, url_prefix='/maintenance')

q = queue.Queue()
p = None
thr = None

@bp.route('update-notochord', methods=['GET'])
def update_notochord():
	global p, thr
	
	try:       
		if p != None and p.isalive():
			return Response(utils.get_response_xml("Notochord is already updating"), mimetype='text/xml'), 202			
			
		update_script_path = path.join(constants.CHORD_MAINTENANCE_DIR, 'update_notochord.sh')
		# spawn a new process as sudo user
		p = PtyProcessUnicode.spawn([update_script_path])
		
		thr = threading.Thread(target=peek_pty_output)
		thr.start()
	except Exception as e:
		res = utils.get_error_xml("Error on notochord update", str(e), e)
		return Response(res, mimetype='text/xml'),500

	res = utils.get_response_xml("Notochord update started")
	return Response(res, mimetype='text/xml'), 202

@bp.route('update-server', methods=['GET'])
def update_server():
	global p, thr
	
	try:       
		if p != None and p.isalive():
			return Response(utils.get_response_xml("Notochord server is already updating"), mimetype='text/xml'), 202			
			
		update_script_path = path.join(constants.CHORD_MAINTENANCE_DIR, 'update_control_server.sh')
		# spawn a new process as sudo user
		p = PtyProcessUnicode.spawn([update_script_path])
		
		thr = threading.Thread(target=peek_pty_output)
		thr.start()
	except Exception as e:
		res = utils.get_error_xml("Error on notochord server update", str(e), e)
		return Response(res, mimetype='text/xml'),500

	res = utils.get_response_xml("Server update started")
	return Response(res, mimetype='text/xml'), 202

@bp.route('update-os', methods=['GET'])
def update_os():
	global p, thr
	
	try:       
		if p != None and p.isalive():
			return Response(utils.get_response_xml("Notochord OS is already updating"), mimetype='text/xml'), 202			
			
		update_script_path = path.join(constants.CHORD_MAINTENANCE_DIR, 'update_noto-os.sh')
		# spawn a new process as sudo user
		p = PtyProcessUnicode.spawn([update_script_path])
		
		thr = threading.Thread(target=peek_pty_output)
		thr.start()
	except Exception as e:
		res = utils.get_error_xml("Error on notochord OS update", str(e), e)
		return Response(res, mimetype='text/xml'),500

	res = utils.get_response_xml("OS update started")
	return Response(res, mimetype='text/xml'), 202

@bp.route('update-all', methods=['GET'])
def update_all():
	global p, thr
	
	try:       
		if p != None and p.isalive():
			return Response(utils.get_response_xml("Notochord is already updating"), mimetype='text/xml'), 202			
			
		update_script_path = path.join(constants.CHORD_MAINTENANCE_DIR, 'update_all.sh')
		# spawn a new process as sudo user
		p = PtyProcessUnicode.spawn([update_script_path])
		
		thr = threading.Thread(target=peek_pty_output)
		thr.start()
	except Exception as e:
		res = utils.get_error_xml("Error on notochord update", str(e), e)
		return Response(res, mimetype='text/xml'),500

	res = utils.get_response_xml("Update started")
	return Response(res, mimetype='text/xml'), 202


def _get_wifi_ssids():
	get_wifi_script_path = path.realpath(path.join(path.dirname(__file__), "get_wifi_ssids.sh"))
	sp = subprocess.run(get_wifi_script_path, capture_output=True)
	res = sp.stdout.decode('utf-8')
	if not res: return jsonify([])
	res = res.split("\n")
	ssids = []
	for line in res:
		if line:
			data = line.split("\t")
			ssids.append({"index": data[0], "ssid": data[1]})

	return ssids

@bp.route('get-wifi-ssids')
def get_wifi_ssids():
	return jsonify(_get_wifi_ssids())

@bp.route('set-wifi-mode')
def set_wifi_mode():
	ssid = request.args.get('ssid')
	pwd = request.args.get('pwd')
	countrycode = request.args.get('countrycode')

	if ssid == None or pwd == None or countrycode == None:
		return Response(utils.get_error_xml("Missing parameters", "SSID, password and country code are required"), mimetype='text/xml'), 400

	global p, thr
	
	try:       
		if p != None and p.isalive():
			return Response(utils.get_response_xml("Already setting wifi..."), mimetype='text/xml'), 202			
			
		update_script_path = path.join(constants.CHORD_MAINTENANCE_DIR, 'set_wifi.sh')
		# spawn a new process as sudo user
		file = "/etc/wpa_supplicant/wpa_supplicant.conf"
		p = PtyProcessUnicode.spawn([update_script_path, file, ssid, pwd, countrycode])
		
		thr = threading.Thread(target=peek_pty_output)
		thr.start()
	except Exception as e:
		res = utils.get_error_xml("Error setting WIFI", str(e), e)
		return Response(res, mimetype='text/xml'),500

	res = utils.get_response_xml("Setting WIFI...")
	return Response(res, mimetype='text/xml'), 202

# @bp.route('set-access-point')
# def set_access_point():
# 	countrycode = request.args.get('countrycode')

# 	if countrycode == None:
# 		return Response(utils.get_error_xml("Missing parameters", "Country code is required"), mimetype='text/xml'), 400

# 	global p, thr
	
# 	try:       
# 		if p != None and p.isalive():
# 			return Response(utils.get_response_xml("Already setting access point..."), mimetype='text/xml'), 202			
			
# 		update_script_path = path.join(constants.CHORD_MAINTENANCE_DIR, 'set_ap.sh')
# 		# spawn a new process as sudo user
# 		p = PtyProcessUnicode.spawn([update_script_path, countrycode])
		
# 		thr = threading.Thread(target=peek_pty_output)
# 		thr.start()
# 	except Exception as e:
# 		res = utils.get_error_xml("Error setting access point", str(e), e)
# 		return Response(res, mimetype='text/xml'),500

# 	res = utils.get_response_xml("Setting access point...")
# 	return Response(res, mimetype='text/xml'), 202

@bp.route('reboot')
def reboot():
	global p, thr
	
	try:       
		if p != None and p.isalive():
			return Response(utils.get_response_xml("Already rebooting..."), mimetype='text/xml'), 202			
			
		update_script_path = 'reboot'
		# spawn a new process as sudo user
		p = PtyProcessUnicode.spawn([update_script_path])
		
		thr = threading.Thread(target=peek_pty_output)
		thr.start()
	except Exception as e:
		res = utils.get_error_xml("Error rebooting", str(e), e)
		return Response(res, mimetype='text/xml'),500

	res = utils.get_response_xml("Rebooting...")
	return Response(res, mimetype='text/xml'), 202

ansi_escape = re.compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')

@bp.route('peek-output', methods=['GET'])
def peek_output():
	state, items = get_subp_output()

	if not state:
		res = utils.get_response_xml("No output available")
		return Response(res, mimetype='text/xml'), 204
	try:
		res = utils.get_queue_xml(items)#get_response_xml("Notochord update output")
		return Response(res, mimetype='text/xml'), 200#return Response(res, mimetype='text/xml'), 200
	except IndexError:
		return Response(utils.get_error_xml("Error retrieving queue"), mimetype='text/xml'), 500

def get_subp_output():
	global q, thr
	items = []
	if q.empty() and (p == None or not p.isalive()):
		if thr != None:
			thr.join()
			thr = None
		return False, items

	while not q.empty():
		items.append(q.get())

	return True, items

def peek_pty_output():
	global q, p, thr	
	recv = []
	try:
		while 1:		
			ch = p.read(1)
			if not ch:
				continue

			if ch == "\n" or ch == "\r":
				if len(recv) > 0:
					result = ansi_escape.sub('', "".join(recv))
					# print(">>>", "".join(recv))
					q.put(result)
				recv = []
			else:
				recv.append(ch)
	except EOFError as e:
		print("---- END ------")
		# Join threads
		p.close()
		p = None