from ctypes import util
import logging
import json
from urllib import request
from flask import Flask, request, Response, Blueprint
from lxml import etree
import os, json
from notochord_control_server import utils, constants
import ipaddress

import notochord.calibration as calibrator

bp = Blueprint('pose', __name__, url_prefix='/pose')

from time import sleep
@bp.route('calibrate', methods=['GET'])
def calibrate():
    step = request.args.get("step", "0")
    run = request.args.get('run', "false")
    run = True if run in ("True", "true", "1") else False

    try:
        # Verify that the step is valid (calibrator.Calibration_Status)
        step = int(step)
        step = calibrator.Calibration_Status(step)
    except KeyError:
        res = utils.get_error_xml("Bad request", "Invalid step")
        return Response(res, mimetype='text/xml'),400

    # Run the calibration step
    calibrator.change_status(step)    
    
    if run:
        return run()

    # Return the status
    label = "Calibration step %s" % step.name
    res = utils.get_response_xml(label)
    
    return Response(res, mimetype='text/xml'),200

@bp.route('run', methods=['GET'])
def run():
    try:
        calibrator.run_calibration()
    except Exception as e:
        res = utils.get_error_xml("Error on notochord calibration", str(e), e)
        return Response(res, mimetype='text/xml'),500
    
    # Return the status
    label = "Calibration run"
    res = utils.get_response_xml(label)
    return Response(res, mimetype='text/xml'),200

@bp.route('data', methods=['GET'])
def data():
    # Get the data
    df = calibrator.get_data()
    print (df)
    # Save the data
    df.to_csv("data.csv")

    # Return the dataframe size
    data = "Dataframe size: %s" % len(df)
    res = utils.get_response_xml(data)
    return Response(res, mimetype='text/xml'),200

@bp.route('index', methods=['GET'])
def index():
    # Get the data
    data = calibrator.get_indexes()
    print (data)
    # Save the data
    with open("data.json", "w") as file:
        file.write(json.dumps({'indexes': data}))

    # Return the dataframe size
    data = ""
    res = utils.get_response_xml(data)
    return Response(res, mimetype='text/xml'),200

@bp.route('<label>.json', methods=['GET','POST','DELETE'])
def animation(label):
    if request.method == 'GET':
        clip_json = utils.read_animation(label) # json
        if not clip_json: # not found error handled on utils.read_animation
            res = utils.get_response_xml("Not found")
            return Response(res, mimetype='text/xml'),404
        else: # clip found, check if valid
            return Response(json.dumps(clip_json), mimetype='application/json'),200
        

    if request.method == 'POST': # json 
        clip_json = request.get_json()
        
        if clip_json:
            utils.write_animation(label, clip_json)
            res = utils.get_response_xml("Animation saved successfully")
            return Response(res, mimetype='text/xml'),200
        else:
            res = utils.get_error_xml("Bad request", "Invalid JSON")
            return Response(res, mimetype='text/xml'),400

@bp.route('', methods=['GET'])
def animations():
    if request.method == 'GET':
        files = os.listdir(constants.ANIMATIONS_PATH)
        animations = utils.get_animations_xml(files)
        return Response(animations, mimetype='text/xml'),200

    # if request.method == 'DELETE':
    #     try:
    #         file_path = path.join(constants.FILES_PATH, "%s.xml" % label)
    #         os.remove(file_path)
    #         res = utils.get_response_xml("Configuration deleted successfully")
    #         return Response(res, mimetype='text/xml'),200
    #     except Exception as e:
    #         logging.exception(e)
    #         res = utils.get_response_xml("Forbidden - %s" % e.strerror)
    #         return Response(res, mimetype='text/xml'),403
