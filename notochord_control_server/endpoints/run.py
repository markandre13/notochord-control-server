from ctypes import util
import logging
from urllib import request
from flask import Flask, request, Response, Blueprint
from lxml import etree
import threading

from notochord_control_server import utils, constants
import ipaddress

from notochord.core import start, end, setup, terminate
from notochord.parse import parse_hierarchy_xml, parse
from notochord.timer import Node_Scheduler

bp = Blueprint('notochord', __name__, url_prefix='/notochord')
   
def start_runtime(c, s):# create a new Node_Scheduler instance
    if c.scan:
        s.scan()
    else:
        xml_filepath = utils.get_active_configuration()
        h = parse_hierarchy_xml(xml_filepath, s, c)

    start(s) # start timer

def setup_notochord(request=None):
    if request:
        args = request.args.get('args', "").split()
    
        restart_comm = request.args.get('restart_comm', "false")
        restart_comm = True if restart_comm in ("True", "true", "1") else False
        
    xml_filepath = utils.get_active_configuration()
    if request:
        args = ["-c",xml_filepath] + args
    else:
        args = ["-c",xml_filepath]
        
    c = parse(args)
    c.comm.log_level = 1
    
    setup(c) # setup notochord

    return c

@bp.route('setup', methods=['GET'])
def notochord_setup():
    try:
        c = setup_notochord(request)

        res = utils.get_notochord_init(c, c.comm.ip, c.comm.port, 0)
        return Response(res, mimetype='text/xml'),200

    except Exception as e:
        res = utils.get_error_xml("Error on notochord setup", str(e))
        return Response(res, mimetype='text/xml'),500

@bp.route('init', methods=['GET'])
def notochord():        
    try:
        c = setup_notochord(request)        

        s = Node_Scheduler() # create a new Node_Scheduler instance

        th = threading.Thread(target=start_runtime, args=(c,s), daemon=True)
        th.start()

        res = utils.get_notochord_init(c, c.comm.ip, c.comm.port, s.get_ODR_modifiers())
        return Response(res, mimetype='text/xml'),202

    except Exception as e:
        res = utils.get_error_xml("Error on notochord initialization", str(e))
        return Response(res, mimetype='text/xml'),500

@bp.route('end', methods=['GET'])
def notochord_end():
    try:
        end() # end timer
        terminate()
        res = utils.get_response_xml("Notochord ended")
        return Response(res, mimetype='text/xml'),200
    except Exception as e:
        res = utils.get_error_xml("Error on notochord termination", str(e), e)
        return Response(res, mimetype='text/xml'),500
