#!/bin/bash

set -eu

_WAIT=0.1
_CMD='pipenv graph'

# Some delayed lines as preelimiary test
for i in {9..0};do
	echo Trying \'$_CMD\' in: $i
	sleep $_WAIT
done

# these lines try to remove color output from pipenv
# export TERM=xterm
# export PIPENV_COLORBLIND=1

# The command to be run
$_CMD

echo Done