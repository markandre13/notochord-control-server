import logging
from urllib import request
from flask import Flask, request, Response, Blueprint
from lxml import etree
import os
from notochord_control_server import utils, state_utils, constants
from notochord.core import status
import notochord.communicator as comm

bp = Blueprint('state', __name__, url_prefix='/state')

# state
@bp.route('', methods=['GET','POST'])
def get_state():
    if request.method == 'GET':
        clear_registry= request.args.get('clear_registry', False) in ("true", "True", "1")
        peek_output= request.args.get('peek_output', False) in ("true", "True", "1")
        
        files = os.listdir(constants.FILES_PATH)
        state = utils.get_state_xml(status(), files, comm.get_registries(clear_registry))

        return Response(state, mimetype='text/xml'),200

    if request.method == 'POST':
        try:
            state = request.data
            state_XML = etree.fromstring(state)
            label = state_XML.find("NotochordConfigurations").text.strip()
            files = [f.rsplit('.',1)[0] for f in os.listdir(constants.FILES_PATH)]
            if label in files:
                state_utils.set_state_var("active_configuration",label)
            else:
                res = utils.get_error_xml("Not Found", "Configuration not found")
                return Response(res, mimetype='text/xml'),404
        except Exception as e:
            
            res = utils.get_error_xml("Internal Server Error", "Error setting state", e)
            return Response(res, mimetype='text/xml'),500
            
        if b"ControlServerState" in state and b"NotochordConfigurations" in state:
            return Response(status=200)
        res = utils.get_response_xml("Bad Request")
        return Response(res, mimetype='text/xml'),400
