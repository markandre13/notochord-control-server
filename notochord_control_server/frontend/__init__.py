from ctypes import util
import logging
from urllib import request
from os import path
import os

import subprocess
from flask import (
	Blueprint, current_app, g, redirect, 
	render_template, request, url_for, 
	make_response, jsonify, session, flash, send_from_directory
)

from lxml import etree

from notochord_control_server import utils, constants, countrycode_list
from notochord_control_server.constants import  __version__
from notochord_control_server.countrycode_list import countrycodes

from notochord.core import status

bp = Blueprint('frontend', __name__, url_prefix='/', static_folder=path.realpath(path.join(path.dirname(__file__), "../notochord-app/dist")))

@bp.route('/assets/<path:path>', methods=['GET'])
def serve_assets(path):
	"""Serve static assets."""
	return send_from_directory(os.path.join(bp.static_folder, 'assets'), path)

@bp.route('/', defaults={'path': ''})
@bp.route('/<path:path>')
def catch_all(path):
	return send_from_directory(bp.static_folder, 'index.html')

@bp.route('/home', methods=['GET'])
def home():
    return render_template('home.html',
                           status=status(),
                           system_info=utils.system_info)    
    
@bp.route('/maintenance', methods=['GET'])
def maintenance():
    countrycode_script = path.realpath(path.join(path.dirname(__file__), "../endpoints/countrycode.sh"))
    countrycode = subprocess.run(countrycode_script, capture_output=True).stdout.decode().strip('\n')
    return render_template('maintenance.html',
                           status=status(),
                           system_info=utils.system_info,
                           countrycode=countrycode,
                           countrycodes=countrycodes)

@bp.route('/pose', methods=['GET'])
def pose():
    return render_template('pose.html',
                           status=status(),
                           system_info=utils.system_info)