import { ref, reactive, inject } from "vue";
import { defineStore } from "pinia";
import { useAssetsStore } from "./assets";
import * as THREE from "three";

export const useAnimationStore = defineStore("animations", () => {
    const notochord = inject('notochord');
    const assets = useAssetsStore();
    const actions = reactive({}); // TODO: it probably should be inside assets
    const mixer = assets.assets.mixer;

    const playing = ref(null);

    function addAction(name, clip) {
        if (assets.assets.mixer === null) {
            console.log("Mixer not initialized");
            return;
        }
        console.log("Adding action");
        console.log(assets.assets.mixer)
        console.log(mixer)
        actions[name] = assets.assets.mixer.clipAction(clip);
        console.log(actions[name])
        return actions[name];
    }

    function play(name=playing.value) {
        if (assets.assets.mixer === null) {
            console.log("Mixer not initialized");
            return;
        }

        if (actions[name] === undefined) {
            console.log("Clip not loaded");
            return;
        }

        stopAll();
        actions[name].play();
        playing.value = name;
    }

    function stopAll() {
        if (assets.assets.mixer === null) {
            console.log("Mixer not initialized");
            return;
        }

        assets.assets.mixer.stopAllAction();
        playing.value = null;
    }

    async function loadAction(name) {
        if (assets.assets.mixer === null) {
            console.log("Mixer not initialized");
            return;
        }

        if (actions[name] === undefined) {
            try {
                console.log("Retrieving clip from server");
                const data = await notochord.http.makeRequest('pose', 'fetchClip', name);
                console.log(data);
                // data.response to JSON
                const jsonClip = JSON.parse(data.response);
                const clip = THREE.AnimationClip.parse(jsonClip);
                addAction(name, clip);
                playing.value = name;
                return actions[name];
            } catch (error) {
                console.log(error);
            }
        } else {
            playing.value = name;
            return actions[name];
        }
    }

    function activeDuration() {
        if (playing.value === null) return 0;      
        return actions[playing.value].getClip().duration;
    }

    return {
        mixer,
        addAction,
        play,
        stopAll,
        loadAction,
        activeDuration,
    }
    
});