import { ref, inject } from 'vue';
import { defineStore } from 'pinia';
import * as THREE from 'three';
import { OrbitControls } from 'three/addons/controls/OrbitControls.js';
import { RoomEnvironment } from 'three/addons/environments/RoomEnvironment.js';
import { GLTFLoader } from 'three/addons/loaders/GLTFLoader.js';
import { RGBELoader } from 'three/addons/loaders/RGBELoader.js';
import hdrURL from '@/assets/studio_small_09_1k.hdr';



export const useAssetsStore = defineStore('assets', () => {
    const notochord = inject('notochord');

    const assets = {}; // { assetName: asset }
    const callbacks = []; // { assetName: callback }

    const usingAvatar = ref(true);
    const sensors = ref({}); // { sensorName: active }

    function init() {
        // Set defaults
        THREE.Object3D.DefaultUp = new THREE.Vector3(0,0,1);
       
        // Create the renderer
        assets.renderer = new THREE.WebGLRenderer({ antialias: true });
        assets.renderer.setPixelRatio(window.devicePixelRatio);
        assets.renderer.setSize(window.innerWidth, window.innerHeight);
        assets.renderer.outputEncoding = THREE.SRGBColorSpace;
        
        // Create the scene
        const pmremGenerator = new THREE.PMREMGenerator(assets.renderer);
        // const hdrURL = new URL(hdrUrl, import.meta.url).href;
        const hdrLoader = new RGBELoader();
        hdrLoader.load(hdrURL, (texture) => {
            const envMap = pmremGenerator.fromEquirectangular(texture).texture;
            assets.scene.environment = envMap;
            assets.scene.fog = new THREE.Fog(0xC7C7C7, 1, 30);
            texture.dispose();
            pmremGenerator.dispose();
            console.log('HDR loaded');
        });

        assets.scene = new THREE.Scene();
        assets.scene.background = new THREE.Color(0xC7C7C7);
        // assets.scene.environment = pmremGenerator.fromScene(new RoomEnvironment(), 0.04).texture;

        // Create the camera
        assets.camera = new THREE.PerspectiveCamera(40, window.innerWidth / window.innerHeight, 1, 100);  
        assets.camera.up.set(0, 0, 1);

        // Create the controls
        assets.controls = new OrbitControls(assets.camera, assets.renderer.domElement);
        assets.controls.object.position.set(2, 2, 2);
        assets.controls.target.set(0, 0, 0);
        assets.controls.enablePan = true;
        assets.controls.enableDamping = true;

        // Create the axes and grid
        assets.axes = new THREE.AxesHelper(5);
        assets.scene.add(assets.axes);
        assets.grid = new THREE.GridHelper(60, 60, 0x414141, 0x414141a1);
        assets.grid.rotateX(Math.PI / 2);
        assets.scene.add(assets.grid);

        // Load the avatar        
        const loader = new GLTFLoader();
        const avatarMesh = new URL('@/assets/avatar.glb', import.meta.url).href;
        loader.load(avatarMesh, (gltf) => {
            console.log('Avatar loaded');
            assets.avatar = gltf.scene;
            assets.avatar.position.set(0, 0, 0);
            assets.avatar.up.set(0, 0, 1);
            assets.avatar.visible = false;
            assets.scene.add(assets.avatar);
            
            // Create the avatar's skeleton
            assets.skeleton = new THREE.SkeletonHelper(assets.avatar);
            assets.skeleton.visible = false;
            assets.scene.add(assets.skeleton);

            // Store the skeleton's bones in a dictionary
            assets.bones = {};
            assets.skeleton.bones.forEach(bone => {
                assets.bones[bone.name] = bone;                
            });

            // Create the mixer
            // assets.mixer = new THREE.AnimationMixer(assets.scene); // try this
            assets.mixer = new THREE.AnimationMixer(assets.avatar);
            assets.actions = {};
        });

        // Load the KCPP
        const kcppMesh = new URL('@/assets/kcpp.glb', import.meta.url).href;
        loader.load(kcppMesh, (gltf) => {
            console.log('KCPP loaded');
            assets.kcpp = gltf.scene;
            assets.kcpp.position.set(0, 0, 0);
            assets.kcpp.up.set(0, 0, 1);
            assets.kcpp.visible = false;
            assets.scene.add(assets.kcpp);
        });        
        
        animate();

        notochord.osc.addCallback('Chordata', '/Chordata/q/*', (data) => {
            // console.log(`Received quaternion ${name}: ${q}`)
            const q = data.args;
            const name = data.address.split('/').pop();
            
            if (!sensors.value[name]) {
                sensors.value[name] = false;
            }
            if (usingAvatar.value) {
                if (assets.bones[name]) {
                    assets.bones[name].quaternion.copy(new THREE.Quaternion(q[1], q[2], q[3], q[0]));
                }
            } else {
                if (assets.kcpp && sensors.value[name]) {
                    assets.kcpp.quaternion.copy(new THREE.Quaternion(q[1], q[2], q[3], q[0]));
                }
            }
        });
    }

    function useAvatar() {
        // If the avatar hasn't been loaded yet, wait for it (set timeout), max 5 tries
        if (!assets.avatar) {
            setTimeout(() => {
                useAvatar();
            }, 200);
            return;
        }
        // Make avatar visible
        assets.avatar.visible = true;
        // assets.skeleton.visible = true;
        usingAvatar.value = true;

        // Hide the KCPP
        if (assets.kcpp)
            assets.kcpp.visible = false;
    }

    function useKCPP() {
        // If the KCPP hasn't been loaded yet, wait for it (set timeout), max 5 tries
        if (!assets.kcpp) {
            setTimeout(() => {
                useKCPP();
            }, 200);
            return;
        }
        // Make KCPP visible
        assets.kcpp.visible = true;
        usingAvatar.value = false;

        // Hide the avatar
        if (assets.avatar) {
            assets.avatar.visible = false;
            assets.skeleton.visible = false;
        }
    }

    function getAsset(assetName) {
        return assets[assetName];
    }

    function animate() {
        requestAnimationFrame(animate);

        callbacks.forEach(cb => cb());

        assets.controls.update();
        assets.renderer.render(assets.scene, assets.camera);
    }

    function setCanvasSize(width, height) {
        assets.camera.aspect = width / height;
        assets.camera.updateProjectionMatrix();
        assets.renderer.setSize(width, height);
    }

    return {
        assets,
        sensors,
        init,
        useAvatar,
        useKCPP,
        getAsset,
        animate,
        setCanvasSize
    };
});