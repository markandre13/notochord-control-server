/**
 * This module exports a Vue plugin that installs the notochord plugin in a Vue app.
 * @module notochord
 */

import { useHttpStore } from '@/modules/notochord/services/http';
import { useConfigStore } from '@/modules/notochord/services/config';
import { useWebsocketStore } from '@/modules/notochord/services/websocket';
import { useOSCStore } from '@/modules/notochord/services/osc';
import { useStatusStore } from '@/modules/notochord/services/status';
import { useCommunicatorStore } from '../store/communicator';

export default {
    /**
     * Installs the notochord plugin in a Vue app.
     * @param {Vue} app - The Vue app instance.
     * @param {Object} options - The plugin options.
     */
    install(app, options) {
        const http = useHttpStore();
        const config = useConfigStore();
        const websocket = useWebsocketStore();
        const osc = useOSCStore();
        const status = useStatusStore();
        const comm = useCommunicatorStore();

        const notochord = {
            http,
            config,
            websocket,
            osc,
            status,
            comm,
        };

        // Provide $notochord in Vue, component & store instances
        app.config.globalProperties.$notochord = notochord;
        app.provide('notochord', notochord);
        
        function init() {
            // Get current HOST
            const host = window.location.hostname;

            osc.init(host);            
            comm.init(osc);
            status.init(osc);      
        }

        /**
         * Setup the notochord plugin.
         * @async
         */
        async function setup() {
            await config.fetchConfigList(true);

        }
        
        init();
        setup();
    }
}