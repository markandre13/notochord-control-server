// TODO: Move to store

import { ref, computed } from 'vue';
import { defineStore } from 'pinia';
import { useHttpStore } from './http';
import { XMLParser, XMLBuilder } from 'fast-xml-parser';
import notochord from '../plugins/notochord';

const serializer = new XMLSerializer();
const regex = /<([^>\s]+)/g; // Lowercase all tags to avoid inconsistencies
const options = {
    numberParseOptions: {
        leadingZeros: true,
        hex: true,
        skipLike: /\+[0-9]{10}/,
        // eNotation: false
    },
    ignoreAttributes: false,
    attributeNamePrefix : "@_",
    allowBooleanAttributes: true
};
const parser = new XMLParser(options);
const builder = new XMLBuilder(options);

function parseXML(xml) {
    const xmlString = serializer.serializeToString(xml);    
    const lowercaseXmlString = xmlString.replace(regex, function(match, p1) {
        return "<" + p1.toLowerCase();
      });   

    return parser.parse(lowercaseXmlString);
}

function buildXML(jsonObject) {    
    return builder.build(jsonObject);
}

export const useConfigStore = defineStore('configuration', () => {
    // TODO: It would be ideal to store the XML and mimic notochord's behaviour
    const configurations = ref({}); // {label: dict, ...}
    const labels = ref([]);
    const activeLabel = ref(null);

    async function fetchConfigList(fetchConfigurations = false) {
        const request = await useHttpStore().makeRequest('state', 'fetch');

        // Get responseXML
        const list = request.responseXML.getElementsByTagName('NotochordConfiguration');
        
        for (let i = 0; i < list.length; i++) {
            const label = list[i].getAttribute('label');
            const isActive = list[i].getAttribute('active') == 'true'
            if (isActive) {
                activeLabel.value = label;
            }
            addLabel(label);
        }

        // console.log('Active label: ', activeLabel.value);
        // console.log('Labels: ', labels.value);

        if (fetchConfigurations) {
            for (let i = 0; i < labels.value.length; i++) {
                const label = labels.value[i];
                await fetchConfig(label);
            }
        }
    }

    async function fetchConfig(label, force = false) {
        if (configurations.value[label] && !force) return configurations.value[label];

        const request = await useHttpStore().makeRequest('configuration', 'fetch', label);
        configurations.value[label] = parseXML(request.responseXML);

        // console.log('Configuration fetched: ', label);
        // console.log('Configuration: ', configurations.value[label]);

        return configurations.value[label];
    }

    async function createConfig(label, payload=null) {
        if (payload == null) {
            payload = buildXML(activeConfig.value);
        }
        const request = await useHttpStore().makeRequest('configuration', 'create', label, payload);
        configurations.value[label] = parseXML(request.responseXML);
        addLabel(label);
    }

    async function deleteConfig(label) {
        const request = await useHttpStore().makeRequest('configuration', 'delete', label);
        delete configurations.value[label];
        labels.value.splice(labels.value.indexOf(label), 1);
    }

    function addLabel(label) {
        if (!labels.value.includes(label)) {
            labels.value.push(label);
        }
    }

    function getConfig(label = null, fetch = false) {
        if (label == null && activeLabel.value != null) {
            label = activeLabel.value;
        } else {
            console.error('No active configuration found. Please set one first.');
            return null;
        }
        if (configurations.value[label]) {
            return configurations.value[activeLabel.value];
        } else if (fetch) {
            return fetchConfig(label);
        }
        console.error('Configuration not found: ', label, '. Fetch it first.');

        return null;
    }

    function setActive(label=activeLabel.value) {
        console.log('Setting active configuration: ', label);
        useHttpStore().makeRequest('state', 'setActiveConfiguration', label);
    }

    // --------------------------------------------------- //
    // Computed active configuration object and attributes //
    // --------------------------------------------------- //

    const activeConfig = computed(() => {
        return configurations.value[activeLabel.value];
    });

    const verbosity = computed({
        get() {
            if (!configurations.value[activeLabel.value].chordata.configuration.communication.verbosity) return 0;        
            return configurations.value[activeLabel.value].chordata.configuration.communication.verbosity
        },
        set(value) {
            configurations.value[activeLabel.value].chordata.configuration.communication.verbosity = value;
        },
    });

    const transmit = computed({
        get() {
            return activeConfig.value.chordata.configuration.communication.transmit;
        },
        set(value) {
            activeConfig.value.chordata.configuration.communication.transmit = value;
        },
    });

    const simulationActive = computed({
        get() {
            if (!activeConfig.value.chordata.configuration.simulation) return false;
            return activeConfig.value.chordata.configuration.simulation.active;
        },
        set(value) {
            activeConfig.value.chordata.configuration.simulation.active = value;
        },
    });

    const csv = computed({
        get() {
            if (!activeConfig.value.chordata.configuration.simulation?.csv_file) return '';
            return activeConfig.value.chordata.configuration.simulation.csv_file;
        },
        set(value) {
            activeConfig.value.chordata.configuration.simulation.csv_file = value;
        },
    });

    const json = computed({
        get() {
            if (!activeConfig.value.chordata.configuration.simulation?.json_file) return '';
            return activeConfig.value.chordata.configuration.simulation.json_file;
        },
        set(value) {
            activeConfig.value.chordata.configuration.simulation.json_file = value;
        },
    });
        
    return {
        configurations,
        labels,
        activeLabel,
        activeConfig,
        verbosity,
        transmit,
        simulationActive,
        csv,
        json,
        fetchConfigList,
        fetchConfig,
        createConfig,
        deleteConfig,
        getConfig,
        setActive,
    }
});