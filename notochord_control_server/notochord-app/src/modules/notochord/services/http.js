import { ref } from 'vue';
import { defineStore } from 'pinia';
import axios from 'axios';

/**
 * The base URL of the API.
 * @type {string}
 */
const API_BASE_URL = '';   // TODO: move to .env or something

// TODO: Perhaps we only need one axios instance with text/xml headers

/**
 * An axios instance with the API base URL and text/xml headers.
 * @type {Object}
 */
const instance = axios.create({
    baseURL: API_BASE_URL,
    timeout: 1000,
    headers: {
        'Content-Type': 'text/xml'
    }
});

/**
 * An axios instance with the API base URL, text/xml headers, and a /configuration endpoint.
 * @type {Object}
 */
const axiosConfiguration = axios.create({
    baseURL: `${API_BASE_URL}/configuration`,
    headers: {
        'Content-Type': 'text/xml'
    }
});

/**
 * Endpoint requests for the API.
 * @type {Object}
 */
const ENDPOINTS = {
    notochord: {
        setup: () => axios.get(`${API_BASE_URL}/notochord/setup`),
        init: () => axios.get(`${API_BASE_URL}/notochord/init`),
        end: () => axios.get(`${API_BASE_URL}/notochord/end`),
    },
    configuration: {
        fetch: (label) => axios.get(`${API_BASE_URL}/configuration/${label}.xml`),
        create: (label, payload) => axiosConfiguration.post(`${label}.xml`, payload),
        delete: (label) => axios.delete(`${API_BASE_URL}/configuration/${label}.xml`),
        setI2CAddr: (addr) => axios.get(`${API_BASE_URL}/configuration/set-i2c-addr?addr=${addr}`),
        calibrate: (step) => axios.get(`${API_BASE_URL}/configuration/calibrate?step=${step}`),
        sensors: () => axios.get(`${API_BASE_URL}/configuration/sensors`),
    },
    pose: {
        calibrate: (step) => axios.get(`${API_BASE_URL}/pose/calibrate?step=${step}`),
        runCalibration: () => axios.get(`${API_BASE_URL}/pose/run`),
        saveClip: (label, json) => axios.post(`${API_BASE_URL}/pose/${label}.json`, json),
        fetchClip: (label) => axios.get(`${API_BASE_URL}/pose/${label}.json`),
        fetchClips: () => axios.get(`${API_BASE_URL}/pose`),
    },
    maintenance: {
        updateAll: () => axios.get(`${API_BASE_URL}/maintenance/update-all`),
        updateNotochord: () => axios.get(`${API_BASE_URL}/maintenance/update-notochord`),
        updateOS: () => axios.get(`${API_BASE_URL}/maintenance/update-os`),
        updateServer: () => axios.get(`${API_BASE_URL}/maintenance/update-server`),
        reboot: () => axios.get(`${API_BASE_URL}/maintenance/reboot`),
    },
    state: {
        fetch: () => axios.get(`${API_BASE_URL}/state`),
        setActiveConfiguration: (label) => {
            const payload = `<ControlServerState><NotochordConfigurations>${label}</NotochordConfigurations></ControlServerState>`;
            return instance.post(`state`, payload);
        },
    },
};

/**
 * A Pinia store for making HTTP requests to the API.
 * @type {Object}
 */
export const useHttpStore = defineStore('http', () => {
    /**
     * A ref to the current request.
     * @type {Object}
     */
    const request = ref(null);

    /**
     * Makes a request to an endpoint with the given request type and parameters.
     * @param {string} endpoint - The endpoint to make the request to.
     * @param {string} requestType - The type of request to make (e.g. 'fetch', 'create', 'delete').
     * @param  {...any} params - The parameters to pass to the request function.
     * @returns {Object} - The request object.
     */
    async function makeRequest(endpoint, requestType, ...params) {
        try {
            const response = await ENDPOINTS[endpoint][requestType](...params);
            request.value = response.request;
            return request.value;
        } catch (error) {
            console.error(error);
        }
    };

    return {
        request,
        makeRequest,
    }
});