import { reactive, ref, toRaw } from 'vue';
import { defineStore } from 'pinia';
import { useStatusStore } from './status';
import OSC, { STATUS } from 'osc-js';

/**
 * Provides a Pinia store for handling OSC communication.
 * @typedef {Object} OSCStore
 * @property {Ref<number>} port - The port number used for OSC communication.
 * @property {Ref<OSC>} osc - The OSC instance used for communication.
 */
export const useOSCStore = defineStore('osc', () => {
    const port = 7681;         // default port, move to constants or env
    
    const sockets = reactive({     // { protocol: socket }
        Chordata: new OSC({ plugin: new OSC.WebsocketClientPlugin({ port: port }) }),
        Chordata_comm: new OSC({ plugin: new OSC.WebsocketClientPlugin({ port: port }) }),
    });        
    const callbacks = reactive({    // { protocol: { address: [callback, callback, ...] } }
        Chordata: {},               // { address: [callback, callback, ...] }
        Chordata_comm: {},
    });
    // TODO: temp solution, need to find a way to remove callbacks
    const callbackIDs = reactive({  // { protocol: { address: [callbackID, callbackID, ...] } }
        Chordata: {},               // { address: [callbackID, callbackID, ...] }
        Chordata_comm: {},
    });

    const status = useStatusStore();

    const reconnecting = reactive({
        Chordata: false,
        Chordata_comm: false,
    });

    /**
     * Initializes the OSC plugin.
     * @param {string} host - The host address to connect to.
     */
    function init(host='127.0.0.1') {       
        for (let socket in sockets) connectSocket(socket, host);
    }

    function connectSocket(socket, host) {
        // console.log(`Connecting to ${host}:${port} via ${socket}`);
        
        sockets[socket].open({ host: host, protocol: socket });

        sockets[socket].on('open', () => {
            // console.log(`OSC ${socket} connection opened`);
            status.setConnection(socket, true);
            reconnecting[socket] = false;
            setCallbacks(socket);
            
        });

        sockets[socket].on('close', () => {
            // console.log(`OSC ${socket} connection closed`);
            status.setConnection(socket, false);
            if (!reconnecting[socket]) checkSocket(socket, host);            
        });

        sockets[socket].on('error', (error) => { 
            // console.error(`Error on socket ${socket}:`, error);
            status.setConnection(socket, false);
            if (!reconnecting[socket]) checkSocket(socket, host);
        });       
        
        
    }

    async function checkSocket(socket, host) {
        console.log(`Attempting to reconnect to ${socket}...`);
        reconnecting[socket] = true;
        setTimeout(() => {
            reconnecting[socket] = false;
            connectSocket(socket, host);
        }, 1000);        
    }

    function setCallbacks(socket) {
        clearCallbacks(socket);

        for (let address in callbacks[socket]) {
            const id = sockets[socket].on(address, callbacks[socket][address][0]);
            callbackIDs[socket][address] = id;            
        }        
    }

    function clearCallbacks(socket) {
        for (let address in callbackIDs[socket]) {
            sockets[socket].off(address, callbackIDs[socket][address]);
        }
    }
    
    function addCallback(socket, address, callback) {
        if (callbacks[socket][address] === undefined) {
            callbacks[socket][address] = [];
        }        
        callbacks[socket][address].push(callback);
        let id = sockets[socket].on(address, callback);
        callbackIDs[socket][address] = id;
    }

    function removeCallback(socket, address, callback) {
        if (callbacks[socket][address] === undefined) return;
        let index = callbacks[socket][address].indexOf(callback);
        if (index < 0) return;
        callbacks[socket][address].splice(index, 1);
        sockets[socket].off(address, callbackIDs[socket][address]);
    }

    function close() {
        for (let socket in sockets) {
            sockets[socket].close();
        }
    }

    return {
        init,
        close,
        addCallback,
        removeCallback        
    };
});