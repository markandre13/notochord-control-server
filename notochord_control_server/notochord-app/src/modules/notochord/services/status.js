import { ref, computed } from 'vue';
import { defineStore } from 'pinia';

const NotochordStatus = {
    IDLE: "IDLE",
    CONFIGURED: "CONFIGURED",
    RUNNING: "RUNNING",
    INACTIVE: "INACTIVE",
}

export const useStatusStore = defineStore('status', () => {
    const connection = ref({}); // { name: 'msg', connected: false }
    const notochordStatus = ref(NotochordStatus.IDLE);
    
    function init(osc) {
        osc.addCallback('Chordata_comm', '/Chordata/comm/status', (message) => {
            console.log('status message: ', NotochordStatus[message.args[0]]);
            setNotochordStatus(NotochordStatus[message.args[0]]);
        });        
    }

    function setConnection(name, connected) {
        connection.value[name] = connected;
    }

    function setNotochordStatus(value) {
        notochordStatus.value = value;
    }

    // Computed
    const connectionStatus = computed(() => {
        // if all connections are connected, then we are connected
        // if at least one connection is connected, then we are 'partially' connected
        // if no connections are connected, then we are not connected
        const connected = Object.values(connection.value).filter(c => c).length;
        if (connected === 0) {
            return 'disconnected';
        }
        if (connected === Object.keys(connection.value).length) {
            return 'connected';
        }
        return 'pending';
    });

    return {
        connection,
        notochordStatus,
        connectionStatus,
        init,
        setConnection,
        setNotochordStatus,
    }
});
