import { ref } from 'vue';
import { defineStore } from 'pinia';
import { useStatusStore } from './status';
import { useHttpStore } from './http';

export const useWebsocketStore = defineStore('websocket', () => {
    const port = ref(7681);         // default port, move to constants or env
    const status = useStatusStore();

    const errorSocket = ref(null);
    const msgSocket = ref(null);
    const errorCallbacks = ref([]);
    const msgCallbacks = ref([]);

    let reconnecting = false;

    function init(host='127.0.0.1') {
        errorSocket.value = connect(host, ["Chordata_error"]);
        msgSocket.value = connect(host, ["Chordata_comm"]);
        setCallbacks();
    }

    function connect(host, protocols) {
        // console.log(`Connecting to ${host}:${port.value}`);
        const ws = new WebSocket(`ws://${host}:${port.value}`, protocols);

        ws.onopen = () => {
            console.log(`Websocket ${protocols} connection opened`);
            status.setConnection('ws: ' + protocols, true);
            reconnecting = false;
        };

        // ws.onmessage = (message) => {
        //     console.log('message: ', message)
        // };

        ws.onclose = (event) => {
            status.setConnection('ws: ' + protocols, false);
            if (event.wasClean) {
                console.log(`WebSocket connection closed cleanly, code: ${event.code}, reason: ${event.reason}`);
            } else {
                console.error('WebSocket connection abruptly closed');
                // Attempt to reconnect
                if (!reconnecting) {
                    console.log('Attempting to reconnect...');
                    reconnecting = true;
                    setTimeout(async () => {
                        clearCallbacks(ws);     
                        // await useHttpStore().makeRequest('notochord', 'setup'); // TODO: only if not already setup     
                        init(host);
                        reconnecting = false;
                    }, 1000);
                }
            }

            
        };

        return ws;
    }

    function setCallbacks() {     
        errorSocket.value.onmessage = (message) => {
            errorCallbacks.value.forEach(cb => cb(message));
        };

        msgSocket.value.onmessage = (message) => {
            msgCallbacks.value.forEach(cb => cb(message));
        };
    }

    function addErrorCallback(cb) {
        errorCallbacks.value.push(cb);
    }

    function addMsgCallback(cb) {
        msgCallbacks.value.push(cb);
    }

    function clearCallbacks(ws) {
        ws.onopen = null;
        ws.onerror = null;
        ws.onmessage = null;
        ws.onclose = null;
    }

    return {
        init,
        addErrorCallback,
        addMsgCallback,
    };
});