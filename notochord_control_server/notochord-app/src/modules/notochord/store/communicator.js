import { ref } from 'vue';
import { defineStore } from 'pinia';

export const useCommunicatorStore = defineStore('communicator', () => {
    const log_types = ['info', 'error', 'debug', 'warning'];
    const messages = ref([]); // {type: 'err', message: 'error message'}

    const limit = 100;
    
    function init(osc) {        
        for (let type of log_types) {
            console.log('type: ', `/Chordata/comm/${type}`)
            osc.addCallback('Chordata_comm', `/Chordata/comm/${type}`, (message) => {
                addMessage(type, message.args[0]);
            });
        }
    }

    function addMessage(type, message) {
        if (messages.value.length >= limit) {
            messages.value.shift(); // remove first element if limit reached
        }
        messages.value.push({type: type, message: message});
    }

    return {
        messages,
        init,
        addMessage,
    };
});
