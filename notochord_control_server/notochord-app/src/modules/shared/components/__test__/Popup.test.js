/**
 * @vitest-environment happy-dom
 */

import { describe, it, expect } from 'vitest'
import { mount, ref } from '@vue/test-utils'
import Popup from '../Popup.vue'

describe('Popup', () => {
  it('emits "saveClip" event when "Save" button is clicked', async () => {
    const wrapper = mount(Popup, {
      props: {
        clipName: 'test clip',
        showPopup: true
      }
    })

    await wrapper.find('button:contains("Save")').trigger('click')

    expect(wrapper.emitted('saveClip')).toBeTruthy()
  })

  it('emits "update:showPopup" event with false when "Discard" button is clicked', async () => {
    // const wrapper = mount(Popup, {
    //   props: {
    //     clipName: 'test clip',
    //     showPopup: true
    //   }
    // })

    // console.log(wrapper.vm.$props.showPopup); // Log the initial value
    // await wrapper.vm.$emit('update:showPopup', false); // Log the event emission
    // await wrapper.vm.$nextTick(); // Log after Vue updates
    // console.log(wrapper.vm.$props.showPopup); // Log the updated value


    // const discard = wrapper.find('button:contains("Discard")')
    // await discard.trigger('click')
    // await wrapper.vm.$nextTick(); // Log after Vue updates
    // const response = wrapper.emitted('update:showPopup')
    // console.log(response)

    // expect(wrapper.emitted('update:showPopup')[0][0]).toBe(false)
  })

  it('emits "update:clipName" event with input value when input is changed', async () => {
    const wrapper = mount(Popup, {
      props: {
        clipName: 'test clip',
        showPopup: true
      }
    })

    const input = wrapper.find('input')
    await input.setValue('new clip name')

    expect(wrapper.emitted('update:clipName')[0][0]).toBe('new clip name')
  })
})