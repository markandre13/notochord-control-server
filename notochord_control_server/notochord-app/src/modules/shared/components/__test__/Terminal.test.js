/**
 * @vitest-environment happy-dom
 */

import { describe, it, expect } from 'vitest'
import { mount } from '@vue/test-utils';
import Terminal from '../Terminal.vue';

describe('Terminal', () => {
  it('renders messages correctly', async () => {
    const wrapper = mount(Terminal, {
      global: {
        provide: {
          notochord: {
            comm: {
              messages: [
                { message: 'Hello world!', type: 'info' },
                { message: 'Error message', type: 'error' },
              ],
            },
          },
        },
      },
    });

    await wrapper.vm.$nextTick();

    const messages = wrapper.findAll('.message');
    expect(messages).toHaveLength(2);

    expect(messages[0].text()).toContain('Hello world!');
    expect(messages[0].classes()).toContain('info');

    expect(messages[1].text()).toContain('Error message');
    expect(messages[1].classes()).toContain('error');
  });
});