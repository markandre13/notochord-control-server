import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'motion',
      component: () => import('@/modules/canvas3d/views/MotionView.vue')
    },
    {
      path: '/cube',
      name: 'cube',
      component: () => import('@/modules/canvas3d/views/CubeView.vue')
    },
    {
      path: '/animate',
      name: 'animate',
      component: () => import('@/modules/canvas3d/views/AnimateView.vue')
    },
    {
      path: '/maintenance',
      name: 'maintenance',
      component: () => import('@/modules/maintenance/views/MaintenanceView.vue')
    },
  ]
})

export default router
