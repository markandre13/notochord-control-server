import json
from os import path
from . import constants

def get_state_var(name):
    check_state_file()

    with open(constants.STATE_PATH, "r") as jsonFile:
        data = json.load(jsonFile)

    return data[name]

def set_state_var(name, value):
    check_state_file()
    
    state_path = constants.STATE_PATH
    with open(state_path, "r") as jsonFile:
        data = json.load(jsonFile)

    data[name] = value

    with open(state_path, "w") as jsonFile:
        json.dump(data, jsonFile)

# Check if the state file exists, if not create it
def check_state_file():
    state_path = constants.STATE_PATH
    if not path.exists(state_path):
        with open(state_path, "w") as jsonFile:
            json.dump({"active_configuration": "default_biped"}, jsonFile)