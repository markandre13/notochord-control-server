window.addEventListener('load', (event) => {

    add_global_handlers();

    document.getElementById ("btn_clear").addEventListener ("click", clear_btn);
    document.getElementById ("btn_update_all").addEventListener ("click", update_all);
    document.getElementById ("btn_update_notochord").addEventListener ("click", update_notochord);
    document.getElementById ("btn_update_server").addEventListener ("click", update_server);
    document.getElementById ("btn_update_os").addEventListener ("click", update_os);
    document.getElementById ("btn_wifi").addEventListener ("click", set_wifi_mode);
    document.getElementById ("btn_reboot").addEventListener ("click", reboot);
    document.getElementById ("btn_reboot_yes").addEventListener ("click", reboot_yes);
    document.getElementById ("btn_reboot_no").addEventListener ("click", reboot_no);
    document.getElementById ("ssid_selector").addEventListener ("click", toggle_ssid_selector);

    function update_all() {
        var url = "/maintenance/update-all";
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", url, true);
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == 4 && xhttp.status == 202)
            {
                get_remote_state.peek_output = true;
            } 
        };
        xhttp.send();
    }

    function update_notochord() {
        var url = "/maintenance/update-notochord";
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", url, true);
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == 4 && xhttp.status == 202)
            {
                get_remote_state.peek_output = true;
            } 
        };
        xhttp.send();
    }

    function update_server() {
        var url = "/maintenance/update-server";
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", url, true);
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == 4 && xhttp.status == 202)
            {
                get_remote_state.peek_output = true;
            } 
        };
        xhttp.send();
    }

    function update_os() {
        var url = "/maintenance/update-os";
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", url, true);
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == 4 && xhttp.status == 202)
            {
                get_remote_state.peek_output = true;
            } 
        };
        xhttp.send();
    }

    function set_wifi_mode() {
        let text_ssid = document.getElementById("ssid_selector").checked;
        let dropdown = document.getElementById("ssid_dropdown");

        // Pick ssid according to ssid_selector state
        var ssid = text_ssid ? document.getElementById("ssid").value : dropdown.options[dropdown.selectedIndex].text;
        var pwd = document.getElementById("pwd").value;
        var countrycode = document.getElementById("countrycode").value;

        // Escape special characters
        ssid = encodeURIComponent(ssid);
        pwd = encodeURIComponent(pwd);
        countrycode = encodeURIComponent(countrycode);

        var url = "/maintenance/set-wifi-mode?ssid="+ssid+"&pwd="+pwd+"&countrycode="+countrycode;
        console.log(url);
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", url, true);
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == 4 && xhttp.status == 202)
            {
                get_remote_state.peek_output = true;
            } 
            if (xhttp.readyState == 4 && xhttp.status != 200)
            {
                var doc = xhttp.responseXML;
                write_to_terminal(doc);
            }
        };
        xhttp.send();
    }

    function reboot() {
        document.getElementById("btn_reboot").hidden = true;
        document.getElementById("reboot-verification").hidden = false;       
    }

    function reboot_yes() {
        var url = "/maintenance/reboot";
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", url, true);
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == 4 && xhttp.status != 200)
            {
                var doc = xhttp.responseXML;
                write_to_terminal(doc);
            }
        };
        xhttp.send();
	document.getElementById("btn_reboot").hidden = false;
        document.getElementById("reboot-verification").hidden = true;
    }

    function reboot_no() {
        document.getElementById("btn_reboot").hidden = false;
        document.getElementById("reboot-verification").hidden = true;
    }

    function toggle_ssid_selector() {
        let sl = document.getElementById("ssid_dropdown");
        let ssid = document.getElementById("ssid");
        if(!document.getElementById("ssid_selector").checked) {
            sl.hidden = false;
            sl.disabled = false;
            ssid.hidden = true;
            ssid.disabled = true;
        }
        else {
            sl.hidden = true;
            sl.disabled = true;
            ssid.hidden = false;
            ssid.disabled= false;
        }
    }

    function get_ssids_names() {
	var url = "/maintenance/get-wifi-ssids";
	var xhttp = new XMLHttpRequest();
	xhttp.open('GET', url, true);
	xhttp.onreadystatechange = function() {
		if(xhttp.readyState == 4) {
			if(xhttp.status == 200) {
                sl = document.getElementById("ssid_dropdown").options;
                ssids = JSON.parse(xhttp.response);
                if(ssids.length > 0) {
                    // Populate ssid list
                    for(i = 0; i < ssids.length; i++) {
                        let opt = document.createElement("option");
                        opt.value = ssids[i].index;
                        opt.text = ssids[i].ssid;
                        sl.add(opt);
                    }
                    toggle_ssid_selector();
                }
                else {
                    // If no networks set the ssid_selector to pick the text input
                    document.getElementById("ssid_selector").checked = true;
                }
			}
            else {
                // If there's an error on the server when retreiving ssids set the
                // ssid_selector to pick the text input
                document.getElementById("ssid_selector").checked = true;
            }
		}
	};
	xhttp.send();
    }
	get_ssids_names();
});
