window.addEventListener('load', (event) => {
    add_global_handlers();

    document.getElementById ("btn_clear").addEventListener ("click", clear_btn);
    
    const calibration_info = document.getElementById("calibration_info");
    var current_step = 0;

    const STEPS = {
        CALIB_IDLE: 0,
        CALIBRATING: 1,
        STATIC: 2,
        ARMS: 3,
        TRUNK: 4,
        L_LEG: 5,
        R_LEG: 6
    }
    
    const step_buttons = [
        null,
        null,
        document.getElementById("btn_static"),
        document.getElementById("btn_arms"),
        document.getElementById("btn_trunk"),
        document.getElementById("btn_l_leg"),
        document.getElementById("btn_r_leg")
    ]

    const step_info = [
        null,
        null,
        document.getElementById("static_step"),
        document.getElementById("arms_step"),
        document.getElementById("trunk_step"),
        document.getElementById("l_leg_step"),
        document.getElementById("r_leg_step"),
        document.getElementById("calibration_done")
    ]

    // Add event listener to connect and disconnect buttons
    document.getElementById ("btn_connect").addEventListener ("click", connect);
    document.getElementById ("btn_disconnect").addEventListener ("click", disconnect);

    document.getElementById ("btn_data").addEventListener ("click", get_data);
    document.getElementById ("btn_index").addEventListener ("click", get_indexes);

    // Add event listeners to the step buttons    
    document.getElementById ("btn_static").addEventListener ("click", function () { change_state(STEPS.STATIC); });
    document.getElementById ("btn_arms").addEventListener ("click", function () { change_state(STEPS.ARMS); });
    document.getElementById ("btn_trunk").addEventListener ("click", function () { change_state(STEPS.TRUNK); });
    document.getElementById ("btn_l_leg").addEventListener ("click", function () { change_state(STEPS.L_LEG); });
    document.getElementById ("btn_r_leg").addEventListener ("click", function () { change_state(STEPS.R_LEG); });

    // Add event listeners to the calibration buttons
    document.getElementById ("btn_calib").addEventListener ("click", start_calibration);
    document.getElementById ("btn_calib_stop").addEventListener ("click", stop_calibration);

    document.getElementById("ext_state").hidden = true;
    document.getElementById("ext_state").previousSibling.hidden = true;
   

    function connect() {
        var addr = document.getElementById("addr").value;
        var port = document.getElementById("port").value;
        var verbose = document.getElementById("verbose").value;
        var url = "/pose/connect?scan=true&addr="+addr+"&port="+port+"&verbose="+verbose;
        get_remote_state.get_registry = true;
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", url, true);
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == 4)
            {
                var doc = xhttp.responseXML;
                write_to_terminal(doc);
            }
        };
        xhttp.send();
    }

    function disconnect() {
        var url = "/pose/disconnect";
        var xhttp = new XMLHttpRequest();
        console.log("Sending request to: " + url);
        xhttp.open('GET', url, true);
        xhttp.onreadystatechange = function() {
            if(xhttp.readyState == 4) {
                if(xhttp.status == 200) {
                    console.log(xhttp.responseText)
                }
                else {
                    console.log("Error: " + xhttp.responseText);
                }
            }
        };
        xhttp.send();
    }

    function get_data() {
        var url = "/pose/data";
        var xhttp = new XMLHttpRequest();
        console.log("Sending request to: " + url);
        xhttp.open('GET', url, true);
        xhttp.onreadystatechange = function() {
            if(xhttp.readyState == 4) {
                if(xhttp.status == 200) {
                    console.log(xhttp.responseText)
                }
                else {
                    console.log("Error: " + xhttp.responseText);
                }
            }
        };
        xhttp.send();
    }

    function get_indexes() {
        var url = "/pose/index";
        var xhttp = new XMLHttpRequest();
        console.log("Sending request to: " + url);
        xhttp.open('GET', url, true);
        xhttp.onreadystatechange = function() {
            if(xhttp.readyState == 4) {
                if(xhttp.status == 200) {
                    console.log(xhttp.responseText)
                }
                else {
                    console.log("Error: " + xhttp.responseText);
                }
            }
        };
        xhttp.send();
    }
    
    function change_state(step) {
        console.log("change_state: " + step);
        // Show or hide the calibration info
        if (calibration_info.style.display === "none") {
            calibration_info.style.display = "block";
        } else if (current_step == step) {
            calibration_info.style.display = "none";
            return;
        }

        // Enable start button
        document.getElementById("btn_calib").disabled = false;

        // Disable stop button
        document.getElementById("btn_calib_stop").disabled = true;

        // Hide all step info
        for (let i = 0; i < step_info.length; i++) {
            if (step_info[i] != null)
                step_info[i].hidden = true;
        }

        // Show the step info for the current step
        step_info[step].hidden = false;

        // Disable all step buttons
        for (let i = 0; i < step_buttons.length; i++) {
            if (step_buttons[i] != null)
                step_buttons[i].disabled = true;
        }

        // Enable the current step button
        if (step_buttons[step] != null)
            step_buttons[step].disabled = false;

        // Set the current step
        current_step = step;         
    }

    function start_calibration() {
        // Disable start button
        document.getElementById("btn_calib").disabled = true;

        // Enable stop button
        document.getElementById("btn_calib_stop").disabled = false;

        // Send the request to start calibration
        var url = "/pose/calibrate?step=" + current_step;
        var xhttp = new XMLHttpRequest();
        console.log("Sending request to: " + url);
        xhttp.open('GET', url, true);
        xhttp.onreadystatechange = function() {
            if(xhttp.readyState == 4) {
                if(xhttp.status == 200) {
                    console.log(xhttp.responseText)
                }
                else {
                    console.log("Error: " + xhttp.responseText);
                }
            }
        };
        xhttp.send();
    }

    function stop_calibration() {        
        // Enable start button
        document.getElementById("btn_calib").disabled = false;

        // Disable stop button
        document.getElementById("btn_calib_stop").disabled = true;

        // Disable the current step button and enable the next step button
        if (current_step < step_buttons.length - 1) {
            step_buttons[current_step].disabled = true;
            step_buttons[current_step + 1].disabled = false;
        } else {
            step_buttons[current_step].disabled = true;
            step_buttons[2].disabled = false;
        }
        var url = "";
        // Send the request to stop calibration
        if (current_step == STEPS.R_LEG) {
            url = "/pose/calibrate?step=" + STEPS.CALIB_IDLE + "&run=1";
        } else {
            url = "/pose/calibrate?step=" + STEPS.CALIB_IDLE;
        }
        var xhttp = new XMLHttpRequest();
        console.log("Sending request to: " + url);
        xhttp.open('GET', url, true);
        xhttp.onreadystatechange = function() {
            if(xhttp.readyState == 4) {
                if(xhttp.status == 200) {
                    console.log(xhttp.responseText)
                }
                else {
                    console.log("Error: " + xhttp.responseText);
                }
            }
        };
        xhttp.send();
    }
});