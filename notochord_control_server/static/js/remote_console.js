function add_global_handlers(){

    const screen_terminal = document.getElementById("terminal");
    const state = document.getElementById("state");
    const ext_state = document.getElementById("ext_state");

    set_collapsibles();

    window.clear_btn = function clear_btn() {
        screen_terminal.innerHTML = "";
    }

    window.write_to_terminal = function write_to_terminal(doc) {
        if (!screen_terminal){
            console.log("Screen terminal not found, cannot write: " + doc);
            return
        } 
        
        if (doc.getElementsByTagName("Log") > 0) {
            doc.getElementsByTagName("Log")[0].childNodes.forEach(append_line_to_terminal);
        } else {
            doc.getElementsByTagName("NotochordResponse")[0].childNodes.forEach(append_line_to_terminal);
        }
    }

    window.write_ws_message = function(msg, cls) {
        if(msg.type == "message") {
            // Remove protocol + ",s" and osc null characters
            var data = msg.data.replace(/\u0000/g, '').substring(msg.srcElement.protocol.length+3);

            // Use the second part of the protocol name as the type of message
            let protocol = msg.srcElement.protocol;
            let type = protocol.split('_')[1];
            append_line(data, type);
        }
        else if(msg.type === "open") {
            append_line(`Websocket connected to host: ${msg.target.url} on protocol ${msg.target.protocol}`);
        }
        else if(msg.type === "error") {
            append_line(`Could not connect to websocket server running on ${msg.target.url}`, "error");
        }
        else if(msg.type === "close") {
            protocol = msg.target.protocol ? `, protocol ${msg.target.protocol}` : "";
            append_line(`Disconnected from websocket server ${msg.target.url}`+protocol);
        }
    }

    function append_line(msg, cls) {
        let p = document.createElement("p");
        p.innerHTML = msg;
        if(cls) {
            p.classList.add(cls);
        }
        screen_terminal.appendChild(p);
        screen_terminal.scrollTop = screen_terminal.scrollHeight;
    }

    const append_line_to_terminal = (element, cls) => {
        if (element.tagName == null) return;
        let tag = document.createElement("p");
        tag.classList.add(element.tagName);
        tag.innerHTML = element.textContent;
        screen_terminal.appendChild(tag);
        screen_terminal.scrollTop = screen_terminal.scrollHeight;
    }

    function switch_buttons(enable){
        if (switch_buttons.disabled && enable){
            switch_buttons.all_btns.forEach((b)=>{b.disabled = false})
            switch_buttons.disabled = false;
        } else if (!switch_buttons.disabled && !enable) {
            switch_buttons.all_btns.forEach((b)=>{b.disabled = true})
            switch_buttons.disabled = true;
        }
    }
    switch_buttons.all_btns = document.querySelectorAll("button");
    switch_buttons.disabled = false; 

    function control_gui(enable=false){
        const delta = 3500;
        const _now = Date.now()
        
        if (enable) control_gui.last_enable = _now;
        
        if (enable || _now - control_gui.last_enable < delta ){
            switch_buttons(true);
            return;
        }

        get_remote_state.peek_output = false;
        get_remote_state.get_registry = false;

        state.innerHTML = "UNAVAILABLE";
        state.classList = ["unavailable"];

        if (ext_state){
            ext_state.innerHTML = state.innerHTML;
            ext_state.classList = state.classList;
        }
        switch_buttons(false);
    }
    control_gui.last_enable = Date.now();
    
    function disable_gui(){control_gui(false)}

    window.get_remote_state = function get_remote_state() {
        var url = `/state?clear_registry=${get_remote_state.get_registry}`;
        if (get_remote_state.peek_output) url += "&peek_output=true";
        var xhttp = new XMLHttpRequest();
        xhttp.timeout = 250;
        xhttp.open("GET", url, true);

        xhttp.onerror = disable_gui;
        xhttp.ontimeout = disable_gui;

        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == 4 ){
                if (xhttp.status == 200){
                    control_gui(true);
                    var doc = xhttp.responseXML;
                    const server_state = doc.getElementsByTagName("ControlServerState");

                    const notochord_process = doc.getElementsByTagName("NotochordProcess");
                    if (!server_state || !notochord_process){
                        console.log("Invalid /state response received");
                        return;
                    }

                    const state_str = notochord_process[0].childNodes[0].nodeValue;
                    state.innerHTML = state_str;
                    state.classList = [state_str.toLowerCase()];

                    doc.querySelector("ControlServerState > Log").childNodes.forEach(append_line_to_terminal);
                    
                    if (doc.querySelector("ExternalProcess").attributes.active.nodeValue == "False"){
                        get_remote_state.peek_output = false;
                        ext_state.innerHTML = "IDLE";
                        ext_state.classList = ["idle"];
                    } else {
                        ext_state.innerHTML = "RUNNING";
                        ext_state.classList = ["running"];
                    }
                    if (get_remote_state.peek_output){
                        doc.querySelector("ExternalProcess > Pty").childNodes.forEach((e) =>{
                            append_line_to_terminal(e, "pty")
                        });
                    }

                    if (state_str != "RUNNING"){
                        console.log("Notochord is not running");
                        document.getElementById ("btn_i2c").disabled = false;
                    } else {               
                        console.log("Notochord is running");       
                        document.getElementById ("btn_i2c").disabled = true;
                    }
                   
                } else {
                   control_gui(false);
                } 
            } 
        };
        xhttp.send();
    }

    const intervalID = setInterval(get_remote_state, 500, screen_terminal);
    get_remote_state.get_registry = false;
    get_remote_state.peek_output = false;

    function set_collapsibles(){
        var coll = document.getElementsByClassName("collapsible");
        var i;
        
        for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function() {
            this.classList.toggle("active");
            let nextSibling = this.nextElementSibling;
            let content = null;
            while( nextSibling  ) {
                if (nextSibling.classList.contains("collapsible-content")){
                    content = nextSibling;
                    break;
                }
                nextSibling = nextSibling.nextElementSibling;
            }
            if (!content) return;
            if (content.style.display === "block") {
                content.style.display = "none";
            } else {
                content.style.display = "block";
            }
        });
        } 
    }




}

