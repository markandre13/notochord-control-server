window.addEventListener('load', (event) => {
    add_global_handlers();
    
    const calibration_info = document.getElementById("calibration_info");
    var current_step = 0;
    
    document.getElementById ("btn_scan").addEventListener ("click", scan);
    document.getElementById ("btn_calib").addEventListener ("click", calibrate);
    document.getElementById ("btn_mag").addEventListener ("click", mag);
    document.getElementById ("btn_save").addEventListener ("click", save);
    document.getElementById ("btn_proceed").addEventListener ("click", proceed);
    document.getElementById ("btn_stop").addEventListener ("click", stop);
    document.getElementById ("btn_i2c").addEventListener ("click", set_i2c);
    document.getElementById ("btn_clear").addEventListener ("click", clear_btn);

    document.getElementById("ext_state").hidden = true;
    document.getElementById("ext_state").previousSibling.hidden = true;

	function scan(e, restart_comm = true, calib = false, raw = false, ws = false) {
		var addr = document.getElementById("addr").value;
		var port = document.getElementById("port").value;
		var verbose = document.getElementById("verbose").value;
		var url = "/notochord/init?scan=1&addr="+addr+"&port="+port+"&verbose="+verbose+"&raw="+raw+"&ws="+ws+"&calib="+calib+"&restart_comm="+restart_comm;
		get_remote_state.get_registry = true;
		var xhttp = new XMLHttpRequest();
		xhttp.open("GET", url, true);
		xhttp.onreadystatechange = function () {
			if (xhttp.readyState == 4)
			{
				if (xhttp.status != 200){
					var doc = xhttp.responseXML;
					write_to_terminal(doc);
				} else {
					if(restart_comm) {
						let host = window.location.hostname;
						let msg_ws = new WebSocket(`ws://${host}:7681`, ["Chordata_comm"]);
						let error_ws = new WebSocket(`ws://${host}:7681`, ["Chordata_error"]);
						msg_ws.onmessage = msg_ws.onopen = msg_ws.onclose = msg_ws.onerror = write_ws_message;
						error_ws.onclose = error_ws.onerror = error_ws.onmessage = error_ws.onopen = write_ws_message;
					}
                }                
            }
        };
        xhttp.send();
    }
    
    function stop() {
        var url = "/notochord/end";
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", url, true);
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == 4)
            {
                var doc = xhttp.responseXML;
                write_to_terminal(doc);
                get_remote_state.get_registry = false;
            }
        };
        xhttp.send();
        current_step = 0;
        document.getElementById("btn_calib").disabled = false;
        document.getElementById("btn_mag").disabled = true;
        document.getElementById("btn_save").disabled = true;
        calibration_info.style.display = "none";
    }

    function calibrate() {
        if (calibration_info.style.display === "none") {
            calibration_info.style.display = "block";
        } else if (current_step == 1) {
            calibration_info.style.display = "none";
            return;
        }
        get_remote_state.get_registry = true;
        document.getElementById("btn_proceed").disabled = false;
        document.getElementById("btn_proceed").hidden = false;
        document.getElementById("calib_step_1").hidden = false;
        document.getElementById("calib_step_2").hidden = true;
        document.getElementById("calib_step_3").hidden = true;
        document.getElementById("calib_step_4").hidden = true;
        current_step = 1;
    }

    function mag() {
        if (calibration_info.style.display === "none") {
            calibration_info.style.display = "block";
        } else if (current_step == 2) {
            calibration_info.style.display = "none";
            return;
        }
        get_remote_state.get_registry = true;
        document.getElementById("btn_proceed").disabled = false;
        document.getElementById("calib_step_1").hidden = true;
        document.getElementById("calib_step_2").hidden = false;
        document.getElementById("calib_step_3").hidden = true;
        document.getElementById("calib_step_4").hidden = true;

        document.getElementById("calib_gif_2").currentTime = 0;
        document.getElementById("calib_gif_2").loop = true;
        document.getElementById("calib_gif_2").play();
        
        current_step = 2;
    }

    function save() {
        if (calibration_info.style.display === "none") {
            calibration_info.style.display = "block";
        } else if (current_step == 3) {
            calibration_info.style.display = "none";
            return;
        }
        
        document.getElementById("btn_proceed").hidden = true;
        document.getElementById("calib_step_1").hidden = true;
        document.getElementById("calib_step_2").hidden = true;
        document.getElementById("calib_step_3").hidden = true;
        document.getElementById("calib_step_4").hidden = false;

        var url = "/configuration/calibrate?state=4";
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", url, true);
        xhttp.onreadystatechange = function() {
            if(xhttp.readyState == 4 && xhttp.status == 200) {
                // Need to set a timeout to get all entries generated during calibration
                // before setting get_registry to false. Otherwise the entries left 
                // will get spammed onto the console
                setTimeout(() => {get_remote_state.get_registry = false;}, 600);
            }
        };
        xhttp.send();

        document.getElementById("btn_save").disabled = true;
        document.getElementById("btn_calib").disabled = false;

        current_step = 3;
    }

    function proceed() {
        document.getElementById("btn_proceed").disabled = true;
        switch (current_step) {
            case 1:                
                scan(null, restart_comm=false, calib=true);

                // Wait for state.innerHTML = "RUNNING" asynchonously, abort if not reached after 5 seconds
                var timeout = 0;
                var interval = setInterval(function() {
                    if (state.innerHTML == "RUNNING") {
                        clearInterval(interval);
                        start_calib();
                    } else if (timeout > 5000) {
                        clearInterval(interval);
                        console.log("Timeout");
                    }
                    timeout += 100;
                }, 100);                    
                break;
            case 2:
                document.getElementById("calib_step_1").hidden = true;
                document.getElementById("calib_step_2").hidden = true;
                document.getElementById("calib_step_3").hidden = false;
                document.getElementById("calib_step_4").hidden = true;

                document.getElementById("calib_gif_3").currentTime = 0;
                document.getElementById("calib_gif_3").loop = true;
                document.getElementById("calib_gif_3").play();
        
                var url = "/configuration/calibrate?state=3";
                var xhttp = new XMLHttpRequest();
                xhttp.open("GET", url, true);
                xhttp.send();
                document.getElementById("btn_mag").disabled = true;
                document.getElementById("btn_save").disabled = false;
                current_step = 0;
                break;
            case 3:
                var url = "/configuration/calibrate?state=4";
                var xhttp = new XMLHttpRequest();
                xhttp.open("GET", url, true);
                xhttp.send();
                document.getElementById("btn_save").disabled = true;
                document.getElementById("btn_calib").disabled = false;
                current_step = 0;
                break;
        }      
    }

    function start_calib() {
        var url = "/configuration/calibrate?state=0";
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", url, true);
        xhttp.send();
        document.getElementById("btn_calib").disabled = true;
        document.getElementById("btn_mag").disabled = false;
    }

    function set_i2c() {                
        var addr = document.getElementById("i2c-addr").value;
        var url = "/configuration/set-i2c-addr?addr="+addr;
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", url, true);
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == 4)
            {
                var doc = xhttp.responseXML;
                write_to_terminal(doc);
            }
        };
        xhttp.send();         
    };

    function set_i2c_addr() {
        var addr = document.getElementById("i2c-addr").value;
        var url = "/configuration/set-i2c-addr?addr="+addr;
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", url, true);
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == 4)
            {
                var doc = xhttp.responseXML;
                write_to_terminal(doc);
            }
        };
        xhttp.send();
    };
    
});
