from lxml import etree
import os, time, stat
from . import state_utils, constants
from .endpoints.maintenance import get_subp_output
from os import path
from flask import url_for
from notochord.parse import read_xml
import re
import traceback


_to_discard = ('path', 'notochord', 'version')
system_info = {'Control server version' : constants.__version__}
for k,v in constants.__dict__.items():
	if k.startswith('_') or k in _to_discard:
		continue
	system_info[k]=v
	
# REGEX to make string XML compatible
xml_compatible = re.compile(u'[^\u0020-\uD7FF\u0009\u000A\u000D\uE000-\uFFFD\U00010000-\U0010FFFF]+')

def get_state_xml(label, files, log):
	root = etree.Element("ControlServerState")
	notochord_process = etree.SubElement(root, "NotochordProcess")
	external_process = etree.SubElement(root, "ExternalProcess")
	notochord_process.text = label

	notochord_configurations = etree.SubElement(root, "NotochordConfigurations")

	active_config = state_utils.get_state_var("active_configuration")
	for f in files:
		file_label = os.path.splitext(f)[0]
		if active_config == file_label:
			is_active = True
		else:
			is_active = False
		config = get_notochord_configuration(file_label, is_active)
		notochord_configurations.append(config)
	
	python_log = etree.SubElement(root, "Log")
	for level, messages in log.items():
		for message in messages:
			partition = message.split("~")
			log_tag = etree.SubElement(python_log, partition[0])
			log_tag.text = partition[1]
			
	subp_state, subp_items = get_subp_output()
	subp_log = etree.SubElement(external_process, "Pty")
	if subp_state:
		for item in subp_items:
			item_node = etree.SubElement(subp_log, "Item")
			# Make string XML compatible
			item = xml_compatible.sub('', item)
			item_node.text = item

	external_process.set("active", str(subp_state))

	state = etree.tostring(root, pretty_print=True)
	return state
	

def get_queue_xml(queue):
	root = etree.Element("NotochordResponse")
	queue_node = etree.SubElement(root, "Queue")
	for item in queue:
		item_node = etree.SubElement(queue_node, "Item")
		item = xml_compatible.sub('', item)
		item_node.text = item
	res = etree.tostring(root, pretty_print=True)
	return res

def get_active_configuration(as_string = False):
	active_config = state_utils.get_state_var("active_configuration")
	file_path = path.realpath(path.join(constants.FILES_PATH, "%s.xml" % active_config))
	if not as_string:
		return file_path

	return read_xml(file_path)
   
def get_response_xml(label):
	root = etree.Element("NotochordResponse")
	state = etree.SubElement(root, "State")
	state.text = label

	res = etree.tostring(root, pretty_print=True)
	return res

def get_error_xml(label, msg, e=None):
	root = etree.Element("NotochordResponse")
	state = etree.SubElement(root, "State")
	state.text = label
	reason = etree.SubElement(root, "Reason")
	reason.text = msg
	trace = etree.SubElement(root, "Trace")
	if e is not None:
		trace.text = traceback.format_exc()

	res = etree.tostring(root, pretty_print=True)
	return res


def read_config(label):
	file_path = path.join(constants.FILES_PATH, "%s.xml" % label)
	try:
		content = etree.parse(file_path)
	except Exception as e:
		return None

	return content


def get_notochord_configuration(label, is_active):
	file_path = path.join(constants.FILES_PATH, "%s.xml" % label)
	url = url_for('configuration.configuration', label=label, _external = True)
	file_stats = os.stat(file_path)
	modification_time = time.ctime(file_stats[stat.ST_MTIME])
 
	root = etree.Element("NotochordConfiguration")
	if is_active:
		root.set('active', 'true')
	else:
		root.set('active', 'false')
	
	root.set('label', label)
	root.set('address', url)
	root.set('date', modification_time)

	return root

# <NotochordConfiguration active="true|false" label="(label)"
#  address="(label).xml" date="22/11/2019, 11.02.59, CET">

def get_notochord_init(config, addr, port, n_sensors):
	root = etree.Element("NotochordResponse")
	start_time = etree.SubElement(root, "StartTime")
	start_time.text = time.ctime() # TODO: Perhaps we should get the start time from notochord module
	root.insert(1, config.to_xml())
	res = etree.tostring(root, pretty_print=True)
	
	return res

import json

def read_animation(label):
	file_path = path.join(constants.ANIMATIONS_PATH, "%s.json" % label)
	try:
		with open(file_path, 'r') as file:
			clip_json = json.load(file)
	except Exception as e:
		return None

	return clip_json

def write_animation(label, clip_json):
	file_path = path.join(constants.ANIMATIONS_PATH, "%s.json" % label)
	with open(file_path, 'w') as file:
		json.dump(clip_json, file)
		
def get_animations_xml(files):
	root = etree.Element("Animations")

	notochord_animations = etree.SubElement(root, "NotochordAnimations")

	# Add each file label
	for f in files:
		file_label = os.path.splitext(f)[0]
		config = get_animation_configuration(file_label)
		notochord_animations.append(config)
	
	animations = etree.tostring(root, pretty_print=True)
	return animations
	
def get_animation_configuration(label):
	file_path = path.join(constants.ANIMATIONS_PATH, "%s.json" % label)
	# url = url_for('pose.animations', label=label, _external = True)
	file_stats = os.stat(file_path)
	modification_time = time.ctime(file_stats[stat.ST_MTIME])
 
	root = etree.Element("NotochordAnimation")
	root.set('label', label)
	# root.set('address', url)
	root.set('date', modification_time)

	return root

def get_sensors_xml(sensors): # return a list of sensor labels
	root = etree.Element("Sensors")
	for sensor in sensors:
		sensor_node = etree.SubElement(root, "Sensor")
		sensor_node.text = sensor.label
	res = etree.tostring(root, pretty_print=True)
	return res