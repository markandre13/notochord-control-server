from setuptools import find_packages, setup
from os import path
import re

class VersionNotFound(Exception):
    pass

def version_from_module():
    filename = path.realpath(path.join(path.dirname(__file__), "notochord_control_server/constants.py"))
    try:
        with open(filename) as f:
            line = f.readline()
            while line:
                res = re.match(r"^__version__\W*=\W['\"](\d+\.\d+\.\d+)['\"]", line)
                if res:
                    return res.group(1)
                else:
                    line = f.readline()
                        
            raise VersionNotFound("Unable to find __version__ in notochord_control_server module")
                
    except FileNotFoundError:
        raise VersionNotFound("Unable to open notochord_control_server module")
    
setup(
    name='notochord_control_server',
    version=version_from_module(),
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask', 'lxml'
    ],
)