import pytest
from notochord_control_server import create_app, constants
from os import path
from time import sleep
from notochord.core import status, terminate, end

# https://flask.palletsprojects.com/en/2.0.x/testing/

@pytest.fixture()
def app():
    app = create_app()
    app.config.update({
        "TESTING": True,
    })

    # other setup can go here

    yield app

    # clean up / reset resources here

def pytest_sessionstart(session):
    constants.FILES_PATH = path.realpath(path.join(path.dirname(__file__), "files"))
    constants.CHORD_MAINTENANCE_DIR = path.realpath(path.join(path.dirname(__file__), "maintenance/"))

@pytest.fixture()
def client(app):
    yield app.test_client()
    counter = 5
    while status() == "CONFIGURED" and counter > 0:
        sleep(0.01)
        counter -= 1
    end()
    terminate()

@pytest.fixture()
def runner(app):
    return app.test_cli_runner()


@pytest.fixture()
def def_biped_path():
    return constants.DEFAULT_BIPED_PATH

@pytest.fixture()
def def_biped():
    with open(constants.DEFAULT_BIPED_PATH) as f:
        xml_string = f.read()
    return xml_string

@pytest.fixture()
def def_biped_forbidden_path():
    return constants.DEFAULT_BIPED_FORBIDDEN_PATH

@pytest.fixture()
def def_biped_forbidden():
    with open(constants.DEFAULT_BIPED_FORBIDDEN_PATH) as f:
        xml_string = f.read()
    return xml_string

@pytest.fixture()
def def_biped_error_path():
    return constants.DEFAULT_BIPED_ERROR_PATH

@pytest.fixture()
def def_biped_error():
    with open(constants.DEFAULT_BIPED_ERROR_PATH) as f:
        xml_string = f.read()
    return xml_string    

@pytest.fixture()
def def_schema():
    with open(constants.SCHEMA_PATH) as f:
        schema_string = f.read()
    return schema_string