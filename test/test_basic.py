from urllib import response
from lxml import etree
from os import path
import os
from time import sleep
from notochord.core import status
import subprocess

# state tests
def test_get_state(client):
    response = client.get("/state")
    assert b"<ControlServerState>" in response.data

def test_post_state(client):
    root = etree.Element("ControlServerState")
    notochord_configurations = etree.SubElement(root, "NotochordConfigurations")
    notochord_configurations.text = "default_biped"
    xml_string = etree.tostring(root, pretty_print=True)
    response = client.post("/state", data=xml_string)
    assert 200 == response.status_code

    notochord_configurations.text = "not_a_configuration"
    xml_string = etree.tostring(root, pretty_print=True)
    response = client.post("/state", data=xml_string)
    assert 404 == response.status_code

# configuration tests
def test_get_configuration(client):
    response = client.get("/configuration/default_biped.xml")
    assert 200 == response.status_code

def test_error_get_configuration(client):
    response = client.get("/configuration/default_biped_error.xml")
    assert 400 == response.status_code

def test_post_and_delete_configuration(client, def_biped_path, def_biped):
    file_path = def_biped_path
    # content = etree.parse(file_path)
	
    # post a new configuration, which creates a new file
    response = client.post("/configuration/post_test.xml", data=def_biped)
    assert 200 == response.status_code

    # delete the created configuration
    response = client.delete("/configuration/post_test.xml")
    assert 200 == response.status_code

def test_post_configuration_errors(client, def_biped_forbidden, def_biped_error):
    #file_path = def_biped_forbidden_path
    # content = etree.parse(file_path)
	
    # post a new configuration with errors
    response = client.post("/configuration/err_conf.xml", data=def_biped_error)
    assert 400 == response.status_code

    #file_path = def_biped_path

    # Make sure the file is not writeable
    subprocess.run(['chmod', '444', "{}/test/files/default_biped_forbidden.xml".format(os.getcwd())], check=True)
    # post a new configuration with permission errors
    response = client.post("/configuration/default_biped_forbidden.xml", data=def_biped_forbidden)
    assert 403 == response.status_code

def test_init_and_end_notochord(client, def_biped, def_schema):	
    # init notochord
    response = client.get("/notochord/init?a=/dev/null")
    assert 202 == response.status_code
    while status() != "RUNNING":
        sleep(0.01)
    # end notochord
    response = client.get("/notochord/end")
    assert 200 == response.status_code


def test_i2c_error(client):
    response = client.get("/configuration/set-i2c-addr?addr=error")
    assert 400 == response.status_code

def test_calib_error(client):
    response = client.get("/configuration/start-calibration?state=-1")
    assert 400 == response.status_code

def test_maintenance_update_notochord(client):
    pass # To be implemented: this produces an infinite loop

    # response = client.get("/maintenance/update-notochord")
    # assert 202 == response.status_code

    # response = client.get("/maintenance/peek-output")
    # assert 200 == response.status_code
    # print(response)
    
    # while response.status_code == 200:    
    #     print(response.data)
    #     response = client.get("/maintenance/peek-output")
        
    # assert 204 == response.status_code