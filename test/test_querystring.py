from urllib import response
from lxml import etree
import pytest
from os import path
import os

from notochord.parse import to_Output_Sink
from notochord.config import Output_Sink

# adapter set to /dev/null so tests can be run on machine without i2c devices
def test_scan(client):
    response = client.get("/notochord/init?a=/dev/null&scan=1")
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    config = xml.find(".//Configuration")
    assert config.find("Scan").text.lower() == 'true'

def test_version(client):
    response = client.get("/notochord/init?a=/dev/null")
    assert 202 == response.status_code
    xml = etree.fromstring(response.data).find("./Chordata")
    assert xml.get("version") == '1.0.2'

def test_msglog(client):
    response = client.get("/notochord/init?a=/dev/null&l=file")
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    comm = xml.find(".//Communication")
    sinks = to_Output_Sink(comm.find("Msg").text.lower())
    # websocket sink is added automatically serverside
    assert sinks == Output_Sink.FILE|Output_Sink.WEBSOCKET
    assert comm.find("Bundles").text.lower() == 'true'

def test_msglog_long(client):
    response = client.get("/notochord/init?a=/dev/null&log=file")
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    comm = xml.find(".//Communication")
    sinks = to_Output_Sink(comm.find("Msg").text.lower())
    # websocket sink is added automatically serverside
    assert sinks == Output_Sink.FILE|Output_Sink.WEBSOCKET

def test_msglog_invalid_option(client):
    response = client.get("/notochord/init?a=/dev/null&l=invalid")
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    comm = xml.find(".//Communication")
    sinks = to_Output_Sink(comm.find("Msg").text.lower())
    # websocket sink is added automatically serverside
    assert sinks == Output_Sink.WEBSOCKET

def test_msglog_multi_short(client):
    response = client.get("/notochord/init?a=/dev/null&l=file,stdout,websocket,osc")
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    comm = xml.find(".//Communication")
    sinks = to_Output_Sink(comm.find("Msg").text.lower())
    assert sinks == Output_Sink.FILE|Output_Sink.STDOUT|Output_Sink.WEBSOCKET|Output_Sink.OSC

def test_msglog_multi_long(client):
    response = client.get("/notochord/init?a=/dev/null&log=file,stdout,websocket")
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    comm = xml.find(".//Communication")
    sinks = to_Output_Sink(comm.find("Msg").text.lower())
    assert sinks  == Output_Sink.STDOUT|Output_Sink.FILE|Output_Sink.WEBSOCKET

def test_odr_119(client):
    response = client.get("/notochord/init?a=/dev/null&odr=119")
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    sensors = xml.find(".//Sensor")
    assert sensors.find("Output_Data_Rate").text == '119'

def test_odr_238(client):
    response = client.get("/notochord/init?a=/dev/null&odr=238")
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    sensors = xml.find(".//Sensor")
    assert sensors.find("Output_Data_Rate").text == '238'

def test_odr_50(client):
    response = client.get("/notochord/init?a=/dev/null&odr=50")
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    sensors = xml.find(".//Sensor")
    assert sensors.find("Output_Data_Rate").text == '50'

def test_odr_invalid(client):
    # default value if option doesn't exists
    odr = 1
    response = client.get("/notochord/init?a=/dev/null&odr={}".format(odr))
    assert 500 == response.status_code
    xml = etree.fromstring(response.data)
    state = xml.find("./State").text
    assert state == 'Error on notochord initialization'
    reason = xml.find("./Reason").text
    assert reason == "pytest: error: argument --odr: invalid odr_validator value: '{}'\n".format(odr)

def test_error_log(client):
    response = client.get("/notochord/init?a=/dev/null&errors=file,stderr,websocket")
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    config = xml.find(".//Configuration")
    comm = config.find("Communication")
    sinks = to_Output_Sink(comm.find("Error").text.lower())
    assert sinks == Output_Sink.STDERR|Output_Sink.FILE|Output_Sink.WEBSOCKET

def test_error_invalid_option(client):
    response = client.get("/notochord/init?a=/dev/null&error=invalid")
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    comm = xml.find(".//Communication")
    sinks = to_Output_Sink(comm.find("Error").text.lower())
    # websocket sink is added automatically serverside
    assert sinks == Output_Sink.WEBSOCKET


def test_transmit_multi(client):
    response = client.get("/notochord/init?a=/dev/null&transmit=stderr,osc")
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    comm = xml.find(".//Communication")
    sinks = to_Output_Sink(comm.find("Transmit").text.lower())
    assert sinks == Output_Sink.STDERR|Output_Sink.OSC

def test_transmit_invalid_option(client):
    response = client.get("/notochord/init?a=/dev/null&transmit=invalid")
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    comm = xml.find(".//Communication")
    assert comm.find("Transmit").text == None

def test_osc_websocket_error(client):
    response = client.get("/notochord/init?a=/dev/null&transmit=websocket,osc")
    assert 500 == response.status_code
    xml = etree.fromstring(response.data)
    reason = xml.find("./Reason").text
    assert reason == 'Cannot set both UDP and WEBSOCKET at the same time'

def test_websocket(client):
    ws_protocol = 'abc'
    ws_port = 5002
    response = client.get("/notochord/init?a=/dev/null&websocket_port={}".format(ws_port))
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    config = xml.find(".//Configuration")
    comm = config.find("Communication")
    assert int(comm.find("WS_Port").text) == ws_port

def test_flags_ones(client):
    response = client.get("/notochord/init?a=/dev/null&scan=1&raw=1&x=1&no_bundles=1&live_mag_correction=1")
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    config = xml.find(".//Configuration")
    sensors = xml.find(".//Sensor")
    comm = xml.find(".//Communication")
    # live_mag_correction is disabled
    #assert sensors.find("live_mag_correction").text.lower() == 'true'
    assert comm.find("Bundles").text.lower() == 'false' # the option is no_bundles which negates this
    assert config.find("Raw").text.lower() == 'true'
    assert config.find("Calibrate").text.lower() == 'true'
    assert config.find("Scan").text.lower() == 'true'
    # madgwick is disabled
    #assert config.find("Madgwick").text.lower() == 'true'

def test_flags_true(client):
    response = client.get("/notochord/init?a=/dev/null&scan=true&raw=true&x=true&no_bundles=true&live_mag_correction=true")
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    config = xml.find(".//Configuration")
    sensors = config.find(".//Sensor")
    comm = config.find(".//Communication")
    # live_mag_correction is disabled
    #assert sensors.find("live_mag_correction").text.lower() == 'true'
    assert comm.find("Bundles").text.lower() == 'false' # the option is no_bundles which negates this
    assert config.find("Raw").text.lower() == 'true'
    assert config.find("Calibrate").text.lower() == 'true'
    assert config.find("Scan").text.lower() == 'true'
    # madgwick is disabled
    #assert config.find("Madgwick").text.lower() == 'true'

def test_flags_false(client):
    response = client.get("/notochord/init?a=/dev/null&r=false&x=false&no_bundles=false&live_mag_correction=false&madgwick=false")
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    config = xml.find(".//Configuration")
    sensors = config.find(".//Sensor")
    comm = config.find(".//Communication")
    # live_mag_correction is disabled
    #assert sensors.find("live_mag_correction").text.lower() == 'true'
    assert comm.find("Bundles").text.lower() == 'true' # the option is no_bundles which negates this
    assert config.find("Raw").text.lower() == 'false'
    assert config.find("Calibrate").text.lower() == 'false'
    # scan needs to be true otherwise the rest of test fail due to an error on notochord termination
    #assert config.find("Scan").text.lower() == 'false'

def test_flags_zero(client):
    response = client.get("/notochord/init?a=/dev/null&r=0&x=0&no_bundles=0&live_mag_correction=0&madgwick=0")
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    config = xml.find(".//Configuration")
    sensors = config.find(".//Sensor")
    comm = config.find(".//Communication")
    # live_mag_correction is disabled
    #assert sensors.find("live_mag_correction").text.lower() == 'true'
    assert comm.find("Bundles").text.lower() == 'true' # the option is no_bundles which negates this
    assert config.find("Raw").text.lower() == 'false'
    assert config.find("Calibrate").text.lower() == 'false'
    # scan needs to be true otherwise the rest of test fail due to an error on notochord termination
    #assert config.find("Scan").text.lower() == 'false'

def test_flags_madgwick(client):
    response = client.get("/notochord/init?madgwick=1")
    assert 500 == response.status_code
    xml = etree.fromstring(response.data)
    state = xml.find("./State").text
    assert state == 'Error on notochord initialization'
    reason = xml.find("./Reason").text
    assert reason == 'The legacy madgwick fusion algorithm is not implemented currently'

def test_adapter_invalid(client):
    response = client.get("/notochord/init?a=/test/invalid_adapter")
    assert 500 == response.status_code
    xml = etree.fromstring(response.data)
    reason = xml.find("./Reason").text
    assert reason == 'The i2c adapter is not available on this system'

def test_adapter(client):
    adapter = '/dev/null'
    response = client.get("/notochord/init?a="+adapter)
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    comm = xml.find(".//Communication")
    assert comm.find("Adapter").text == adapter

def test_log_file(client):
    f = 'test/log.txt'
    response = client.get('/notochord/init?a=/dev/null&f='+f)
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    comm = xml.find(".//Communication")
    assert comm.find("Log_file").text == "{}/{}".format(os.getcwd(), f)

def test_base_osc(client):
    base = 'test'
    response = client.get("/notochord/init?a=/dev/null&b="+base)
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    osc = xml.find(".//Osc")
    assert osc.find("Base").text == base

def test_comm_osc(client):
    base = '/test' # set the base address to to not prevent the test from breaking if defaults change
    comm_osc = '/comm_osc_test'
    response = client.get("/notochord/init?a=/dev/null&b="+base+"&comm_osc="+comm_osc)
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    osc = xml.find(".//Osc")
    assert osc.find("Msg").text == base+comm_osc

def test_error_osc(client):
    base = '/test' # set the base address to to not prevent the test from breaking if defaults change
    error_osc = '/error_osc_test'
    response = client.get("/notochord/init?a=/dev/null&b="+base+"&error_osc="+error_osc)
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    osc = xml.find(".//Osc")
    assert osc.find("Error").text == base+error_osc

def test_send_rate(client):
    response = client.get("/notochord/init?a=/dev/null&s=100")
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    comm = xml.find(".//Communication")
    rate = float(comm.find("Send_rate").text) 
    assert pytest.approx(rate) == 100.0

def test_verbose(client):
    response = client.get("/notochord/init?a=/dev/null&v=1")
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    config = xml.find(".//Communication")
    assert config.find("Verbosity").text == '1'

def test_verbose_invalid_float(client):
    verbose = 2.3
    response = client.get("/notochord/init?a=/dev/null&v="+str(verbose))
    assert 500 == response.status_code
    xml = etree.fromstring(response.data)
    state = xml.find("./State").text
    assert state == 'Error on notochord initialization'
    reason = xml.find("./Reason").text
    assert reason == "pytest: error: argument -v/--verbose: invalid verbosity_validator value: '{}'\n".format(verbose)

def test_verbose_invalid_string(client):
    verbose = "abc"
    response = client.get("/notochord/init?a=/dev/null&v="+str(verbose))
    assert 500 == response.status_code
    xml = etree.fromstring(response.data)
    state = xml.find("./State").text
    assert state == 'Error on notochord initialization'
    reason = xml.find("./Reason").text
    assert reason == "pytest: error: argument -v/--verbose: invalid verbosity_validator value: '{}'\n".format(verbose)

def test_verbose_invalid_short(client):
    verbose = 5
    response = client.get("/notochord/init?a=/dev/null&v="+str(verbose))
    assert 500 == response.status_code
    xml = etree.fromstring(response.data)
    state = xml.find("./State").text
    assert state == 'Error on notochord initialization'
    reason = xml.find("./Reason").text
    assert reason == "pytest: error: argument -v/--verbose: invalid verbosity_validator value: '{}'\n".format(verbose)

def test_verbose_invalid(client):
    verbose = 5
    response = client.get("/notochord/init?a=/dev/null&verbose="+str(verbose))
    assert 500 == response.status_code
    xml = etree.fromstring(response.data)
    state = xml.find("./State").text
    assert state == 'Error on notochord initialization'
    reason = xml.find("./Reason").text
    assert reason == "pytest: error: argument -v/--verbose: invalid verbosity_validator value: '{}'\n".format(verbose)

def test_ip_port(client):
    ip = '192.168.1.255'
    port = 8000
    response = client.get("/notochord/init?a=/dev/null&addr="+ip+"&port="+str(port))
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    comm = xml.find(".//Communication")
    assert comm.find("Ip").text == ip
    assert int(comm.find("Port").text) == port

def test_invalid_port(client):
    port = 500000
    response = client.get("/notochord/init?a=/dev/null&port="+str(port))
    assert 400 == response.status_code
    xml = etree.fromstring(response.data)
    state = xml.find("./State").text
    assert state == 'Bad Request'
    reason = xml.find("./Reason").text
    assert reason == "The requested port ({}) is out of range".format(port)

def test_invalid_ip(client):
    ip = '192.168.1.300'
    response = client.get("/notochord/init?a=/dev/null&addr="+ip)
    assert 400 == response.status_code
    xml = etree.fromstring(response.data)
    state = xml.find("./State").text
    assert state == 'Bad Request'
    reason = xml.find("./Reason").text
    assert reason == "'{}' does not appear to be an IPv4 or IPv6 address".format(ip)

def test_addr_as_ip(client):
    ip = '192.168.1.255'
    response = client.get("/notochord/init?a=/dev/null&ip="+ip)
    assert 202 == response.status_code
    xml = etree.fromstring(response.data)
    comm = xml.find(".//Communication")
    assert comm.find("Ip").text == '127.0.0.1'
